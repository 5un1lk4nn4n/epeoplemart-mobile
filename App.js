/* eslint-disable no-console */
/* eslint-disable func-names */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {StatusBar, View} from 'react-native';
import ThemeProvider from './app/utils/ThemeProvider';
import AppNavigator from './app/navigations/AppNavigator';
import {NetworkProvider} from './app/utils/NetworkProvider';
import {Colors} from './app/styles';
// console.disableYellowBox = true;
class App extends React.Component {
  constructor(props) {
    super(props);

    // console.log(uuid, 'uuid');
  }

  render() {
    return (
      <ThemeProvider>
        <NetworkProvider>
          <StatusBar
            backgroundColor={Colors.APP.light}
            barStyle="dark-content"
          />
          <AppNavigator />
        </NetworkProvider>
      </ThemeProvider>
    );
  }
}

export default App;
