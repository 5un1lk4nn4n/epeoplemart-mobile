//export const DOMAIN = 'http://192.168.43.137:8000/';
//export const MAIN_URL = 'http://192.168.43.137:8000/api/users/';
//export const MEDIA_URL = 'http://192.168.43.137:8000/api/files/assets/';
//export const ACCOUNTS_URL = 'http://192.168.43.137:8000/api/accounts/';

export const DOMAIN = 'https://api.epeoplemart.com/';
export const MAIN_URL = 'https://api.epeoplemart.com/api/users/';
export const MEDIA_URL = 'https://api.epeoplemart.com/static/media/';
export const ACCOUNTS_URL = 'https://api.epeoplemart.com/api/accounts/';

export const URLS = {
  HOME: 'home',
  QUICK_PAY: 'authosire-sale',
  PAY_COMPLETE: 'finalise',
  CATEGORIES: 'categories',
  SUGGESTION: 'suggestion',
  SEARCH: 'search',
  NEARME: 'around-me',
  LOGIN: 'auth/login',
  VENDOR_VIEWS: 'vendor/views/',
  VENDOR: 'vendors',
};
