import React from 'react';
import {Image} from 'react-native';
import {Block, Text, Input, Button} from 'galio-framework';
// import {SearchBar} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Colors} from '../styles';
import TopNavBar from '../components/TopNavBar';
import Ripple from 'react-native-material-ripple';
import Icon from 'react-native-vector-icons/Feather';
import Share from 'react-native-share';
import AsyncStorage from '@react-native-community/async-storage';

const shareOptions = {
  title: 'Share via',
  message: 'Know about all your offers',
  url: 'www.epeoplemart.com',
};

class Accounts extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };

  state = {
    search: '',
    userName: 'Login',
    token: null,
  };
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    this.userToken = await AsyncStorage.getItem('@token');
    this.userName = await AsyncStorage.getItem('@name');
    this.setState({
      userName: this.userName ? this.userName : 'Login for premium access',
      token: this.userName,
    });
  }

  updateSearch = search => {
    this.setState({search});
  };

  render() {
    const {userName, token} = this.state;
    return (
      <Block
        flex={1}
        style={{
          backgroundColor: Colors.APP.light,
          alignItems: 'flex-start',
        }}>
        <Block
          row
          space="between"
          width={wp('90%')}
          style={{
            alignItems: 'center',
            alignSelf: 'center',
            marginTop: hp('5%'),
            marginBottom: hp('3%'),
          }}>
          <Block>
            <Text h4>{userName}</Text>
            <Text muted>Premium User</Text>
          </Block>
          <Icon
            name="user"
            color={'#666'}
            size={wp('6%')}
            style={{
              backgroundColor: Colors.APP.white,
              padding: wp('2%'),
              borderRadius: wp('3%'),
            }}
          />
          {/* <Image
            resizeMode="contain"
            source={{
              uri: 'https://randomuser.me/api/portraits/men/44.jpg',
            }}
            style={{
              width: wp('15%'),
              height: wp('15%'),
              borderRadius: wp('7.5%'),
            }}
          /> */}
        </Block>
        <Block
          style={{
            backgroundColor: 'white',
            marginVertical: hp('2%'),
          }}>
          {/* <Ripple
            style={{
              width: wp('100%'),
              height: hp('5%'),
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: wp('5%'),
            }}
            onPress={() => this.props.navigation.navigate('History')}>
            <Icon
              name="activity"
              color={'#666'}
              size={wp('5%')}
              style={{
                paddingRight: wp('1%'),
              }}
            />
            <Text>Purchase History</Text>
          </Ripple> */}
          <Ripple
            style={{
              width: wp('100%'),
              height: hp('5%'),
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: wp('5%'),
            }}
            onPress={() => {}}>
            <Icon
              name="key"
              color={'#666'}
              size={wp('4%')}
              style={{
                paddingRight: wp('1%'),
              }}
            />
            <Text>Change Password</Text>
          </Ripple>
        </Block>
        <Block
          style={{
            backgroundColor: 'white',
          }}>
          {/* <Ripple
            style={{
              width: wp('100%'),
              height: hp('5%'),
              flexDirection: 'row',
              //   justifyContent: 'center',
              alignItems: 'center',
              //   backgroundColor: 'red',
              paddingHorizontal: wp('5%'),
            }}
            onPress={() => alert('hi')}>
            <Icon
              name="settings"
              color={'#666'}
              size={wp('4%')}
              style={{
                paddingRight: wp('1%'),
              }}
            />
            <Text>Settings</Text>
          </Ripple> */}
          <Ripple
            style={{
              width: wp('100%'),
              height: hp('5%'),
              flexDirection: 'row',
              //   justifyContent: 'center',
              alignItems: 'center',
              //   backgroundColor: 'red',
              paddingHorizontal: wp('5%'),
            }}
            onPress={() => alert('hi')}>
            <Icon
              name="life-buoy"
              color={'#666'}
              size={wp('4%')}
              style={{
                paddingRight: wp('1%'),
              }}
            />
            <Text>Help & Support</Text>
          </Ripple>
          <Ripple
            style={{
              width: wp('100%'),
              height: hp('5%'),
              flexDirection: 'row',
              //   justifyContent: 'center',
              alignItems: 'center',
              //   backgroundColor: 'red',
              paddingHorizontal: wp('5%'),
            }}
            onPress={async () => {
              const userName = await AsyncStorage.getItem('@name');
              Share.open({
                message: `Hi, ${userName} here... I want you too get benefited by ePeopleMart.com's great offers & complimentaries. Grab it @ https://www.epeoplemart.com/offers`,
              })
                .then(res => {
                  console.log(res);
                })
                .catch(err => {
                  err && console.log(err);
                });
            }}>
            <Icon
              name="share-2"
              color={'#666'}
              size={wp('4%')}
              style={{
                paddingRight: wp('1%'),
              }}
            />
            <Text>Invite Friends</Text>
          </Ripple>
        </Block>

        <Block
          style={{
            backgroundColor: 'white',
            marginTop: hp('2%'),
          }}>
          <Ripple
            style={{
              width: wp('100%'),
              height: hp('5%'),
              flexDirection: 'row',
              //   justifyContent: 'center',
              alignItems: 'center',
              //   backgroundColor: 'red',
              paddingHorizontal: wp('5%'),
            }}
            onPress={() => {
              if (token) {
                AsyncStorage.clear();
                this.setState({
                  token: null,
                  userName: 'Login for premium access',
                });
              } else {
                this.props.navigation.navigate('SignIn');
              }
            }}>
            <Icon
              name="log-out"
              color={'#666'}
              size={wp('4%')}
              style={{
                paddingRight: wp('1%'),
              }}
            />
            <Text>{token ? 'Log Out' : 'Login'}</Text>
          </Ripple>
          {!token ? (
            <Ripple
              style={{
                width: wp('100%'),
                height: hp('5%'),
                flexDirection: 'row',
                //   justifyContent: 'center',
                alignItems: 'center',
                //   backgroundColor: 'red',
                paddingHorizontal: wp('5%'),
              }}
              onPress={() => {
                if (token) {
                  AsyncStorage.clear();
                  this.setState({
                    token: null,
                    userName: 'Login for premium access',
                  });
                } else {
                  this.props.navigation.navigate('SignIn');
                }
              }}>
              <Icon
                name="help-circle"
                color={'#666'}
                size={wp('4%')}
                style={{
                  paddingRight: wp('1%'),
                }}
              />
              <Text> Forgot Password?</Text>
            </Ripple>
          ) : null}
        </Block>
        <Block
          style={{
            backgroundColor: 'white',
            marginTop: hp('2%'),
          }}>
          {!token ? (
            <Ripple
              style={{
                width: wp('100%'),
                height: hp('5%'),
                flexDirection: 'row',
                //   justifyContent: 'center',
                alignItems: 'center',
                //   backgroundColor: 'red',
                paddingHorizontal: wp('5%'),
              }}
              onPress={() => {
                if (token) {
                  AsyncStorage.clear();
                  this.setState({
                    token: null,
                    userName: 'Login for premium access',
                  });
                } else {
                  this.props.navigation.navigate('SignIn');
                }
              }}>
              <Icon
                name="heart"
                color={'#666'}
                size={wp('4%')}
                style={{
                  paddingRight: wp('1%'),
                }}
              />
              <Text>New User? Register Here</Text>
            </Ripple>
          ) : null}
        </Block>
      </Block>
    );
  }
}

export default Accounts;
