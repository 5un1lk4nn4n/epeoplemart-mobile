import React from 'react';
import {StyleSheet, KeyboardAvoidingView, StatusBar} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {View} from 'react-native-animatable';
// import {showMessage} from 'react-native-flash-message';

// import Button from '../ui_components/button';
import {Block, Button, Input, Text, NavBar, Icon} from 'galio-framework';
import {login as goLogin} from '../services/api';
import {Colors, Typography} from '../styles';
import Toast from '../utils/Toast';
import {NetworkContext} from '../utils/NetworkProvider';

// const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 40;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    ...Typography.ff.medium,
    justifyContent: 'space-around',
  },

  textbox: {
    width: wp('80%'),
    borderBottomColor: Colors.APP.primary,
    color: Colors.APP.primary,
    textAlign: 'center',
    margin: hp('3%'),
    ...Typography.fs.secondary,
    ...Typography.ff.medium,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: hp('2%'),
  },
  nextBtn: {
    marginTop: hp('4%'),
  },

  btnLabel: {
    color: Colors.APP.white,
    alignSelf: 'center',
    ...Typography.fs.primary,
    ...Typography.ff.medium,
  },
  infoMsg: {
    color: Colors.APP.greyer,
    ...Typography.fs.secondary,
    margin: hp('2%'),
    ...Typography.ff.light,
    alignSelf: 'center',
  },
});

export default class Login extends React.Component {
  static navigationOptions = {
    header: null,
  };

  static contextType = NetworkContext;

  message = null;

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      loading: false,
      mobile: null,
      name: null,
    };
  }

  showNext = () => {
    const {name, mobile} = this.state;
    const {isConnected} = this.context;
    if (!name || !mobile) {
      //   showMessage({
      //     message: 'Both mobile and name are required ',
      //     type: 'default',
      //     backgroundColor: Colors.APP.base, // background color
      //     color: Colors.APP.black,
      //   });
      return;
    }
    if (!isConnected) {
      //   showMessage({
      //     message: 'You are offline!',
      //     type: 'default',
      //     backgroundColor: Colors.APP.base, // background color
      //     color: Colors.APP.black,
      //   });
      return;
    }
    this.setState({loading: true});
    goLogin({name, mobile}, this.login);
  };

  login = data => {
    this.setState({loading: false});
    const {name, mobile} = this.state;
    if (data.code === 200) {
      this.showToast(data.message);
      const {navigation} = this.props;
      navigation.navigate('Otp', {name, mobile});
    } else if (data.code === 401 || data.code === 400) {
      //   showMessage({
      //     message: data.message,
      //     type: 'default',
      //     backgroundColor: Colors.APP.base, // background color
      //     color: Colors.APP.black,
      //   });
    }
  };

  hideToast = () => {
    this.setState({
      visible: false,
    });
  };

  showToast = msg => {
    this.message = msg;
    this.setState(
      {
        visible: true,
      },
      () => {
        this.hideToast();
      },
    );
  };

  handleNameChange = val => {
    this.setState({
      name: val,
    });
  };

  handleMobileChange = val => {
    this.setState({
      mobile: val,
    });
  };

  renderForm = () => {
    return (
      <Block style={{backgroundColor: Colors.APP.base}}>
        <Text>Logi</Text>
        <Input
          placeholder="Your Name"
          type="default"
          autoCapitalize="none"
          icon="user"
          family="AntDesign"
          autoCorrect={false}
          ref={this.nameRef}
          returnKeyType="next"
          borderless
          placeholderTextColor={Colors.APP.black}
          onChangeText={val => this.handleNameChange(val)}
          // onSubmitEditing={() => {
          //   if (this.nameRef.current) this.mobileRef.current.focus();
          // }}
          style={{
            width: wp('80%'),
            borderBottomColor: Colors.APP.black,
            color: Colors.APP.black,
            textAlign: 'center',
            ...Typography.ff.bold,
            borderBottomWidth: 1,
            fontWeight: 'normal',
            // marginBottom: hp('2%'),
          }}
        />
        <Input
          type="number-pad"
          placeholder="Your Mobile Number"
          icon="mobile1"
          placeholderTextColor={Colors.APP.black}
          borderless
          family="AntDesign"
          autoCapitalize="none"
          returnKeyType="done"
          style={{
            width: wp('80%'),
            borderBottomColor: Colors.APP.black,
            color: Colors.APP.black,
            textAlign: 'center',
            borderBottomWidth: 1,
          }}
          ref={this.mobileRef}
          maxLength={10}
          onSubmitEditing={() => this.showNext()}
          onChangeText={val => this.handleMobileChange(val)}
        />
      </Block>
    );
  };

  render() {
    const {visible, loading} = this.state;

    return (
      <KeyboardAvoidingView style={styles.container} behavior="height" enabled>
        <Block flex={0.4}>{/* <LogoCard /> */}</Block>
        <Block flex={0.5} center>
          {this.renderForm()}
          <Button
            loading={loading}
            color={Colors.APP.primary}
            style={styles.nextBtn}
            onPress={this.showNext}
            size="small"
            radius={15}>
            <Block row middle>
              <Text
                color={Colors.APP.black}
                style={{
                  ...Typography.ff.bold,
                  marginRight: wp('6%'),
                }}>
                GET OTP
              </Text>
              <Icon
                name="arrow-right"
                size={wp('4%')}
                family="Feather"
                color={Colors.APP.black}
              />
            </Block>
          </Button>
          <Block width={wp('80%')} style={{marginTop: hp('4%')}}>
            <Text
              color={Colors.APP.black}
              style={{
                ...Typography.ff.semiBold,
              }}>
              By tapping Get OTP, I agree to QBee's Terms of Service, Payment
              Terms of Service and Privacy Policy.
            </Text>
          </Block>
        </Block>

        <Toast visible={visible} message={this.message} />
      </KeyboardAvoidingView>
    );
  }
}
