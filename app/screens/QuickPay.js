import React from 'react';
import {Image, Linking, TouchableOpacity, StyleSheet, View} from 'react-native';
import {Block, Text, Input, Button} from 'galio-framework';
import * as Animatable from 'react-native-animatable';
import LottieView from 'lottie-react-native';
import ApiService from '../services/api';
import {withNavigationFocus} from 'react-navigation';

// import {SearchBar} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Colors} from '../styles';
import TopNavBar from '../components/TopNavBar';
import Icon from 'react-native-vector-icons/Feather';
import {ScrollView} from 'react-native-gesture-handler';

import SearchOption from '../components/SearchOption';
const bgImage = require('../assets/images/bk1.png');
const premiumUser = require('../assets/images/premium.png');
const Failed = require('../assets/images/authFailed.png');
const successPay = require('../assets/images/success.jpg');
import Carousel from 'react-native-snap-carousel';
import Modal from 'react-native-modal';
import {RNCamera} from 'react-native-camera';
import Loader from '../components/Loader';

const okAnimation = require('../assets/animations/ok.json');
const scanningline = {
  from: {
    height: 0,
  },
  to: {
    height: wp('49.5%'),
  },
};

Animatable.initializeRegistryWithDefinitions({
  scanningline,
});
const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  container: {
    flex: 1,
    // flexDirection: 'column',
    backgroundColor: Colors.APP.light,
  },
  preview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  overlay: {
    backgroundColor: 'rgba(255,255,255,0.2)',
  },
  contentRow: {
    flexDirection: 'row',
  },
  content: {
    borderWidth: 2,
    borderColor: Colors.APP.secondary,
  },
  scanline: {
    borderBottomColor: Colors.APP.light,
    borderBottomWidth: 2,
  },
});

class QuickPay extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };

  api = new ApiService();

  state = {
    isSearch: false,
    scanned: true,
    isModalVisible: false,
    success: false,
  };
  componentDidMount() {
    // Or set a specific startFrame and endFrame with:
    // alert('hi');
  }

  onSuccess = e => {
    Linking.openURL(e.data).catch(err =>
      console.error('An error occured', err),
    );
  };

  showScanningLine = () => {
    return (
      <Animatable.View
        animation="scanningline"
        iterationCount="infinite"
        style={styles.scanline}
        direction="alternate"
      />
    );
  };

  showTouchToScan = scanned => {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: 'rgba(255,255,255,0.8)',
          width: wp('50%'),
          height: wp('50%'),
        }}
        onPress={() => this.setState({scanned})}>
        <Text
          style={{
            color: Colors.APP.black,
            textAlign: 'center',
            textAlignVertical: 'center',
            lineHeight: wp('55%'),
          }}>
          Tap to scan
        </Text>
      </TouchableOpacity>
    );
  };

  componentWillUnmount() {
    // this.animation.reset();
    // alert('hi');
  }

  handleBarCodeScanned = ({barcodes}) => {
    const {data} = barcodes[0];
    this.setState({
      scanned: false,
      isModalVisible: true,
      loader: true,
    });

    this.authoriseUser(data);

    // alert(data);
  };

  authoriseUser = code => {
    this.api
      .quickPay(code)
      .then(res => {
        console.log(res.is_authenticated);
        if (!res.is_authenticated) {
          this.authorise = res;
          this.setState({
            loader: false,
          });
        }

        if (res.code === 400) {
        }
        if (res.code === 401) {
          this.setState({
            loader: false,
          });
          this.isFailed = True;
        }
        if (res.code === 200) {
          this.authorise = res.data;
          this.setState({
            loader: false,
          });
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
        this.setState({
          loader: false,
        });
      });
  };

  renderQuickPay = () => {
    const {success} = this.state;
    const {navigation} = this.props;
    if (!this.authorise.is_authenticated) {
      return (
        <>
          <Image
            resizeMode="contain"
            source={Failed}
            style={{
              marginTop: hp('4%'),
              width: wp('30%'),
              height: wp('30%'),
            }}
          />
          <Text p color={Colors.APP.primary}>
            Authentication Failed. Login and try if you didn't login
          </Text>
          <Button
            color="transparent"
            onPress={() => {
              this.toggleModal();
              // this.animation.reset();
              navigation.navigate('Home');
            }}
            style={{
              borderWidth: 0,
              width: wp('30%'),
            }}>
            <Text color={Colors.APP.primary}>Back to Home</Text>
          </Button>
        </>
      );
    }
    if (success) {
      return (
        <Block
          row
          style={{
            marginVertical: hp('2%'),
          }}>
          <Block center>
            {/* <LottieView
              ref={animation => {
                this.animation = animation;
              }}
              style={{
                width: wp('20%'),
                height: wp('20%'),
              }}
              loop={false}
              source={okAnimation}
            /> */}
            <Image
              resizeMode="contain"
              source={successPay}
              style={{
                marginTop: hp('4%'),
                width: wp('20%'),
                height: wp('20%'),
                borderRadius: wp('10%'),
              }}
            />
            <Text muted p>
              Thank you for shopping with us
            </Text>
            <Button
              color="transparent"
              onPress={() => {
                this.toggleModal();
                // this.animation.reset();
                navigation.navigate('Home');
              }}
              style={{
                borderWidth: 0,
                width: wp('30%'),
              }}>
              <Text color={Colors.APP.primary}>Back to Home</Text>
            </Button>
          </Block>
        </Block>
      );
    }
    return (
      <>
        <Text h4>{this.authorise.shop}</Text>
        <Input
          placeholder="Amount Paid"
          color={Colors.APP.base}
          style={{
            borderColor: Colors.APP.base,
            borderWith: 0,
            width: wp('90%'),
          }}
          placeholderTextColor={Colors.APP.base}
          onChange={val => {
            this.amountPaid = val;
          }}
        />
        <Block
          row
          center
          style={{
            width: wp('80%'),
            marginTop: hp('1%'),
            alignSelf: 'center',
            justifyContent: 'space-between',
          }}>
          <Button
            round
            onPress={() => {
              if (!this.amountPaid) {
                return;
              }
              this.setState(
                {
                  success: true,
                },
                () => {
                  // this.animation.play();
                },
              );
            }}
            size="small"
            style={{
              width: wp('30%'),
              marginRight: wp('2%'),
            }}>
            Avail Offer
          </Button>
          <Button
            size="small"
            round
            color={Colors.APP.primary}
            onPress={() => {
              const filters = {
                search: this.authorise.shop,
              };
              this.setState(
                {
                  isModalVisible: false,
                },
                () => {
                  this.props.navigation.navigate('ListOffer', filters);
                },
              );
            }}
            style={{
              borderColor: Colors.APP.primary,
              width: wp('30%'),
            }}>
            <Text color={Colors.APP.white}>View Store Offers</Text>
          </Button>
        </Block>
        <Block>
          <Button
            size="small"
            round
            color="transparent"
            onPress={this.toggleModal}
            style={{
              borderColor: Colors.APP.primary,
              width: wp('30%'),
              marginTop: hp('2%'),
            }}>
            <Text color={Colors.APP.primary}>Cancel</Text>
          </Button>
        </Block>
      </>
    );
  };

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  render() {
    const {success, isSearch, scanned, isModalVisible, loader} = this.state;
    const {navigation} = this.props;
    const isFocused = this.props.navigation.isFocused();
    if (isSearch) {
      return <SearchOption />;
    }
    return (
      <Block
        flex={1}
        style={{
          backgroundColor: Colors.APP.light,
        }}>
        <View style={styles.container}>
          {isFocused ? (
            <RNCamera
              ref={ref => {
                this.camera = ref;
              }}
              captureAudio={false}
              style={styles.preview}
              type={RNCamera.Constants.Type.back}
              flashMode={RNCamera.Constants.FlashMode.on}
              androidCameraPermissionOptions={{
                title: 'Permission to use camera',
                message: 'We need your permission to use your camera',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              onGoogleVisionBarcodesDetected={
                scanned ? this.handleBarCodeScanned : null
              }>
              <View style={{flex: 1}}>
                <View style={[styles.overlay, {height: hp('30%')}]}>
                  <Block
                    center
                    style={{
                      height: hp('10%'),
                      padding: wp('5%'),
                      width: wp('100%'),
                      backgroundColor: Colors.APP.light,
                      borderBottomLeftRadius: 20,
                      borderBottomRightRadius: 20,
                    }}>
                    <Text h3 color={Colors.APP.black}>
                      Scan QR Code
                    </Text>
                    <Text muted color={Colors.APP.black}></Text>
                  </Block>
                </View>
                <View style={[styles.contentRow, {height: wp('50%')}]}>
                  <View
                    style={[
                      styles.overlay,
                      {width: wp('25%'), height: wp('50%')},
                    ]}
                  />
                  <View style={[{width: wp('50%'), height: wp('50%')}]}>
                    {scanned
                      ? this.showScanningLine()
                      : this.showTouchToScan(!scanned)}
                  </View>
                  <View
                    style={[
                      styles.overlay,
                      {width: wp('25%'), height: wp('50%')},
                    ]}
                  />
                </View>

                <View style={[styles.overlay, {height: hp('40%')}]}></View>
              </View>
            </RNCamera>
          ) : null}

          {isModalVisible ? (
            <Modal
              isVisible={isModalVisible}
              style={{
                justifyContent: 'flex-end',
                margin: 0,
                flex: 1,
                width: wp('100%'),
              }}>
              <Block
                style={{
                  flex: 0.8,
                  backgroundColor: Colors.APP.light,
                  width: wp('100%'),
                }}
                center>
                {!loader && this.authorise.is_authenticated ? (
                  <Image
                    resizeMode="contain"
                    source={premiumUser}
                    style={{
                      marginTop: hp('4%'),
                      width: wp('30%'),
                      height: wp('30%'),
                    }}
                  />
                ) : null}

                {loader ? <Loader /> : this.renderQuickPay()}
              </Block>
            </Modal>
          ) : null}
        </View>
      </Block>
    );
  }
}

export default withNavigationFocus(QuickPay);
