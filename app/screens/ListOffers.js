import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  View,
  StyleSheet,
  PermissionsAndroid,
} from 'react-native';
import {MEDIA_URL} from '../constants';
import getDirections from 'react-native-google-maps-directions';
import Search from './Search';

import Ripple from 'react-native-material-ripple';
import Dash from 'react-native-dash';

import {Block, Text, Input, Button} from 'galio-framework';
import Geolocation from 'react-native-geolocation-service';
// import {SearchBar} from 'react-native-elements';
import Modal from 'react-native-modal';
import {NetworkContext} from '../utils/NetworkProvider';
import Offline from '../components/Offline';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import MapView, {Marker} from 'react-native-maps';
import {Colors} from '../styles';
import TopNavBar from '../components/TopNavBar';
import Icon from 'react-native-vector-icons/Feather';

import SearchOption from '../components/SearchOption';
const bgImage = require('../assets/images/bk1.png');
import Carousel from 'react-native-snap-carousel';
import Loader from '../components/Loader';
import ApiService from '../services/api';

const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = 0.01;
const discount = require('../assets/images/discount.png');
const bag = require('../assets/images/bag.png');
const offer1 = require('../assets/images/1.jpg');
const offer2 = require('../assets/images/2.jpg');
const offer3 = require('../assets/images/3.jpg');
const offer4 = require('../assets/images/4.jpg');
const offer5 = require('../assets/images/5.jpg');
const offer6 = require('../assets/images/6.jpg');

const CATEGORIES = ['Restautent', 'Supermarket', 'Mobile', 'Clothing'];

const initialRegion = {
  latitude: 22.3511148,
  longitude: 78.6677428,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
};

const styles = StyleSheet.create({
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  backBtn: {
    height: hp('6%'),
    width: hp('6%'),
    borderRadius: hp('3%'),
  },

  cone: {
    width: 0,
    height: 0,
    borderLeftWidth: wp('5%'),
    borderLeftColor: 'transparent',
    borderRightWidth: wp('5%'),
    borderRightColor: 'transparent',
    borderTopWidth: wp('9%'),
    borderTopColor: Colors.APP.base,
    borderRadius: wp('5%'),
    position: 'absolute',
    left: 17,
    top: 42,
    zIndex: 4,
  },
});

const markers = [
  {
    latitude: 11.009986,
    longitude: 76.94961,
    title: 'Hotel Ethiran',
    subtitle: '1234 Foo Drive',
  },
  {
    latitude: 11.009812,
    longitude: 76.950162,
    title: 'Nalli Silks',
    subtitle: '1234 Foo Drive',
  },
];

class ListOffer extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };
  static contextType = NetworkContext;
  api = new ApiService();

  currentShop = null;

  state = {
    latitude: 0,
    longitude: 0,
    error: null,
    ready: false,
    activeShopName: null,
    activeCategory: 'All',
    isMapView: false,
    isModalVisible: false,
    loader: true,
    search: this.props.navigation.getParam('search'),
    category: this.props.navigation.getParam('category'),
    location: this.props.navigation.getParam('location'),
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.navigation.getParam('search') !== prevState.search ||
      nextProps.navigation.getParam('category') !== prevState.category ||
      nextProps.navigation.getParam('location') !== prevState.location
    ) {
      // loadOffers();

      return {
        search: nextProps.navigation.getParam('search'),
        category: nextProps.navigation.getParam('category'),
        location: nextProps.navigation.getParam('location'),
      };
    } else return null;
  }

  componentDidUpdate(prevProps, prevState) {
    const {error} = this.state;
    if (
      prevProps.navigation.getParam('search') !== prevState.search ||
      prevProps.navigation.getParam('category') !== prevState.category ||
      prevProps.navigation.getParam('location') !== prevState.location
    ) {
      this.loadOffers();
    }
  }

  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage

    this.requestLocationPermission();
    this.loadOffers();
  }

  loadOffers = () => {
    const isToday = this.props.navigation.getParam('isToday');
    const search = this.props.navigation.getParam('search');
    const category = this.props.navigation.getParam('category');
    const location = this.props.navigation.getParam('location');
    const vendorId = this.props.navigation.getParam('vendor_id');

    const filters = {
      is_today: isToday,
      search,
      category,
      location,
      vendor_id: vendorId,
    };

    console.log(filters);
    this.api
      .searchOffers(filters)
      .then(res => {
        console.log('over');
        if (res.code === 400) {
        }
        if (res.code === 200) {
          this.offer = res.data;
          this.setState({
            loader: false,
          });
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  updateViews = vendorId => {
    this.api
      .vendorViewCount(vendorId)
      .then(res => {
        if (res.code === 400) {
        }
        if (res.code === 200) {
          this.loadOffers();
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };

  updateSearch = search => {
    this.setState({search});
  };

  _renderItem = ({item, index}) => {
    return (
      <Block
        style={{
          backgroundColor: 'white',
          height: wp('30%'),
          width: wp('30%'),
          borderRadius: 20,
          marginRight: wp('2%'),
          marginTop: hp('2%'),
        }}></Block>
    );
  };

  componentWillUnmount() {
    this.watchID != null && Geolocation.clearWatch(this.watchID);
  }

  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'This App needs access to your location ' +
            'so we can know where you are.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use locations ');
        this.getCurrentPosition();
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  setView = type => {
    this.setState({
      isMapView: type,
    });
  };

  getCurrentPosition = () => {
    console.log('location access');
    this.watchID = Geolocation.getCurrentPosition(
      position => {
        const lat = parseFloat(position.coords.latitude);
        const long = parseFloat(position.coords.longitude);
        console.log('location', lat, long);
        this.setState({
          latitude: lat,
          longitude: long,
          ready: true,
        });
        // this.initCheckin();
      },
      error => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000},
    );
  };

  handleGetDirections = key => {
    const {latitude, longitude} = this.state;
    console.log(this.currentShop, 'ssd');
    const data = {
      source: {
        latitude,
        longitude,
      },
      destination: {
        latitude: parseFloat(this.currentShop.location.latitude),
        longitude: parseFloat(this.currentShop.location.longitude),
      },
    };

    getDirections(data);
  };

  filterShop = category => {
    var newList = DATA.filter(function(el) {
      return el.category === category.toLowerCase();
    });
    return newList;
  };
  render() {
    const {
      ready,
      latitude,
      longitude,
      shopList,
      isMapView,
      isModalVisible,
      error,
      loader,
      submitLoader,
      successFlag,
      activeShopName,
      activeCategory,
    } = this.state;
    const {navigation} = this.props;
    const {isConnected} = this.context;

    if (!isConnected) {
      return <Offline navigation={navigation} />;
    }

    if (loader) {
      return <Loader />;
    }
    return (
      <Block
        center
        // row
        flex={1}
        style={{
          // height: hp('30%'),
          // width: wp('100%'),
          backgroundColor: Colors.APP.light,
        }}>
        <Search navigation={this.props.navigation} />

        {/* <Block row center>
          <Button
            color="transparent"
            onPress={() => {
              navigation.goBack();
            }}
            style={{
              alignItems: 'center',
              backgroundColor: Colors.APP.primary,
              width: wp('10%'),
              borderWidth: 0,
              alignItems: 'flex-start',
              padding: wp('2%'),
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
              borderTopRightRadius: 0,
              borderBottomRightRadius: 0,
            }}>
            <Icon name="arrow-left" color={Colors.APP.white} size={wp('5%')} />
          </Button>
          <Button
            color="transparent"
            onPress={() => {
              navigation.navigate('Search');
            }}
            style={{
              backgroundColor: '#fff',
              width: wp('80%'),
              borderWidth: 0,
              alignItems: 'flex-start',
              paddingLeft: wp('2%'),
              borderTopRightRadius: 10,
              borderBottomRightRadius: 10,
              borderTopLeftRadius: 0,
              borderBottomLeftRadius: 0,
              marginTop: hp('2%'),
              marginBottom: hp('2%'),
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Icon name="search" color={Colors.APP.base} size={wp('5%')} />
            <Text muted>
              {'  '}
              Search Shops, Categories or Locations...
            </Text>
          </Button>
        </Block> */}

        <Block
          flex={1}
          style={{
            backgroundColor: Colors.APP.light,
          }}>
          <View style={{}}>
            {this.offer.length ? (
              <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}>
                {this.offer.map((shop, key) => {
                  return (
                    <Ripple
                      onPress={() => {
                        this.updateViews(shop.vendor);
                        this.currentShop = shop;
                        this.toggleModal();
                      }}
                      style={{
                        flexDirection: 'row',
                        width: wp('90%'),
                        backgroundColor: Colors.APP.white,
                        marginBottom: hp('2%'),
                        justifyContent: 'flex-start',
                        borderRadius: 20,
                        paddingVertical: hp('1%'),
                        paddingLeft: wp('2%'),
                      }}>
                      <Image
                        source={{
                          uri: `${MEDIA_URL}${shop.banner}`,
                        }}
                        resizeMode="contain"
                        style={{
                          width: wp('30%'),
                          // backgroundColor: 'yellow',

                          height: wp('30%'),
                          // borderTopLeftRadius: 20,
                          // borderBottomLeftRadius: 20,
                        }}
                      />
                      <Block
                        style={{
                          // backgroundColor: 'yellow',
                          height: wp('30%'),
                          paddingVertical: hp('1%'),
                          marginLeft: wp('2%'),
                        }}>
                        <Text p bold>
                          {shop.title.length > 22
                            ? `${shop.title.substring(0, 22)}...`
                            : shop.title}
                        </Text>
                        <Text muted>
                          {shop.description.length > 28
                            ? `${shop.description.substring(0, 28)}...`
                            : shop.description}
                        </Text>
                        <Block
                          row
                          style={{
                            alignItems: 'center',
                          }}>
                          <Text
                            h5
                            style={{
                              marginRight: wp('1%'),
                            }}>
                            {`${parseFloat(shop.discount.toString())}%`}
                          </Text>
                          <Text>{'discount'}</Text>
                        </Block>
                        <Text
                          style={{
                            // backgroundColor: '#004B9385',
                            // padding: wp('1%'),
                            borderRadius: 15,
                            marginTop: hp('1%'),
                          }}
                          bold
                          color={Colors.APP.primary}>
                          {`Valid till ${shop.to_date}`}
                        </Text>
                        <Block
                          row
                          style={{
                            alignItems: 'center',
                          }}>
                          <Icon name="eye" size={wp('3%')} style={{}} />
                          <Text
                            style={{
                              // backgroundColor: '#004B9385',
                              // padding: wp('1%'),
                              borderRadius: 15,
                              marginLeft: wp('1%'),
                            }}
                            bold
                            color={Colors.APP.black}>
                            {`${shop.views}`}
                          </Text>
                        </Block>
                      </Block>

                      <Icon
                        name="arrow-right"
                        size={wp('5%')}
                        style={{
                          position: 'absolute',
                          right: 10,
                          top: 10,
                        }}
                      />
                    </Ripple>
                  );
                })}

                <Block
                  style={{
                    height: hp('6%'),
                    backgroundColor: Colors.APP.light,
                  }}></Block>
              </ScrollView>
            ) : (
              <Block>
                <Text muted p>
                  Nothing Found
                </Text>
              </Block>
            )}
          </View>
        </Block>
        {isModalVisible ? (
          <Modal
            isVisible={isModalVisible}
            style={{
              justifyContent: 'flex-end',
              margin: 0,
              flex: 1,
              width: wp('100%'),
            }}>
            <Block
              style={{
                flex: 0.7,
                backgroundColor: Colors.APP.light,
                width: wp('100%'),
              }}
              center>
              <Block
                row
                style={{
                  position: 'absolute',
                  top: -10,
                }}>
                <Block
                  style={{
                    height: 20,
                    width: 10,
                    borderTopRightRadius: 50,
                    borderBottomRightRadius: 50,
                    backgroundColor: Colors.APP.greyer,
                  }}
                />
                <Dash
                  style={{width: wp('96%'), height: 1, alignSelf: 'center'}}
                  dashColor={Colors.APP.greyer}
                />
                <Block
                  style={{
                    height: 20,
                    width: 10,
                    borderTopLeftRadius: 50,
                    borderBottomLeftRadius: 50,
                    backgroundColor: Colors.APP.greyer,
                  }}
                />
              </Block>
              <Image
                source={discount}
                resizeMode="contain"
                style={{
                  marginVertical: hp('1%'),
                  width: wp('15%'),
                  height: wp('15%'),
                }}
              />
              <Text p bold>
                {this.currentShop.vendor_shop_name}
              </Text>
              <Text>{this.currentShop.address}</Text>
              <Block row>
                <Dash
                  style={{
                    width: wp('100%'),
                    height: 1,
                    marginVertical: hp('1%'),
                    alignSelf: 'center',
                  }}
                  dashColor={Colors.APP.white}
                />
              </Block>
              <Text
                color={Colors.APP.primary}
                bold
                style={{
                  backgroundColor: '#004B9385',
                  padding: wp('1%'),
                  width: wp('30%'),
                  borderRadius: 15,
                  marginTop: hp('1%'),
                  textAlign: 'center',
                  position: 'absolute',
                  right: 10,
                  top: 10,
                }}>
                {this.currentShop.category}
              </Text>
              <Text
                style={{
                  backgroundColor: '#004B9385',
                  padding: wp('1%'),
                  width: wp('30%'),
                  borderRadius: 15,
                  marginTop: hp('1%'),
                  textAlign: 'center',
                  position: 'absolute',
                  left: 10,
                  top: 10,
                }}
                bold
                color={Colors.APP.primary}>
                {`Valid till ${this.currentShop.to_date}`}
              </Text>
              <Block
                center
                style={
                  {
                    // backgroundColor: 'yellow',
                  }
                }>
                <Text p>{this.currentShop.title}</Text>
                <Text>{this.currentShop.description}</Text>
                <Text
                  h4
                  bold
                  style={{
                    marginTop: hp('1%'),
                  }}>{`${parseFloat(
                  this.currentShop.discount.toString(),
                )}% OFF`}</Text>
                <Block row>
                  <Block
                    style={{
                      height: 20,
                      width: 10,
                      borderTopRightRadius: 50,
                      borderBottomRightRadius: 50,
                      backgroundColor: Colors.APP.greyer,
                    }}
                  />
                  <Dash
                    style={{width: wp('96%'), height: 1, alignSelf: 'center'}}
                    dashColor={Colors.APP.greyer}
                  />
                  <Block
                    style={{
                      height: 20,
                      width: 10,
                      borderTopLeftRadius: 50,
                      borderBottomLeftRadius: 50,
                      backgroundColor: Colors.APP.greyer,
                    }}
                  />
                </Block>

                <Block
                  style={{
                    marginVertical: hp('2%'),
                    width: wp('100%'),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Button
                    round
                    onPress={() => {
                      this.currentShop = null;
                      this.toggleModal();
                      navigation.navigate('QuickPay');
                    }}
                    color="primary"
                    style={{
                      width: wp('80%'),
                      height: hp('4%'),
                      borderColor: Colors.APP.base,
                      marginVertical: 10,
                    }}>
                    <Text color={Colors.APP.white}>Scan QR to avail offer</Text>
                  </Button>

                  <Button
                    round
                    onPress={() => {
                      this.handleGetDirections();
                      // this.currentShop = null;
                      // this.toggleModal();
                    }}
                    color={Colors.APP.primary}
                    style={{
                      width: wp('80%'),
                      height: hp('4%'),
                      borderColor: Colors.APP.base,
                      padding: 5,
                    }}>
                    <Text color={Colors.APP.white}>Get Direction on Gmaps</Text>
                  </Button>
                </Block>
              </Block>
              <Block
                style={{
                  width: wp('100%'),
                  paddingHorizontal: wp('2%'),
                }}>
                {/* <Text>Other Offers in this Shop</Text> */}
              </Block>
              <Button
                onPress={() => {
                  this.currentShop = null;
                  this.toggleModal();
                }}
                color="transparent"
                size="small"
                style={{
                  borderWidth: 0,
                  borderWidth: 1,
                  borderColor: Colors.APP.primary,
                  elevation: 0,
                  width: wp('20%'),
                  height: hp('4%'),
                  borderRadius: 20,
                }}>
                <Text color={Colors.APP.primary}>Back</Text>
              </Button>
            </Block>
          </Modal>
        ) : null}
      </Block>
    );
  }
}

export default ListOffer;
