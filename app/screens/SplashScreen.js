import React from 'react';
import {Image} from 'react-native';
import {Block, Text} from 'galio-framework';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import LottieView from 'lottie-react-native';

import {Colors} from '../styles/';
const logo = require('../assets/images/logo.png');
const banner = require('../assets/images/banner.png');
const loader = require('../assets/animations/loader.json');

class SplashScreen extends React.Component {
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    this.animation.play();

    const data = await this.performTimeConsumingTask();
    const {navigation} = this.props;

    if (data !== null) {
      navigation.navigate('AuthLoading');
    }
  }

  performTimeConsumingTask = async () => {
    return new Promise(resolve =>
      setTimeout(() => {
        resolve('result');
      }, 2000),
    );
  };

  render() {
    return (
      <Block
        flex={1}
        style={{
          backgroundColor: Colors.APP.light,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={logo}
          resizeMode="contain"
          style={{
            width: wp('70%'),
            height: hp('6%'),
          }}
        />
        <Text bold color={Colors.APP.primary}>
          Explore your offers
        </Text>
        <LottieView
          ref={animation => {
            this.animation = animation;
          }}
          style={{
            width: wp('50%'),
            height: wp('50%'),
          }}
          // loop={false}
          source={loader}
        />

        <Image
          source={banner}
          resizeMode="contain"
          style={{
            position: 'absolute',
            bottom: 0,
            width: wp('100%'),
            height: hp('7%'),
          }}
        />
      </Block>
    );
  }
}

export default SplashScreen;
