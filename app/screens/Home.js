import React from 'react';
import {
  Image,
  ImageBackground,
  FlatList,
  PermissionsAndroid,
} from 'react-native';
import {Block, Text, Input, Button} from 'galio-framework';
// import {SearchBar} from 'react-native-elements';
import {MEDIA_URL} from '../constants';
import Dash from 'react-native-dash';
import getDirections from 'react-native-google-maps-directions';
import AsyncStorage from '@react-native-community/async-storage';
import Geolocation from 'react-native-geolocation-service';
import Ripple from 'react-native-material-ripple';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Colors} from '../styles';
import TopNavBar from '../components/TopNavBar';
import Icon from 'react-native-vector-icons/Feather';
import Modal from 'react-native-modal';
import {ScrollView} from 'react-native-gesture-handler';
import {NetworkContext} from '../utils/NetworkProvider';
import Offline from '../components/Offline';

import ApiService from '../services/api';
const discount = require('../assets/images/discount.png');
import Search from './Search';
import SearchOption from '../components/SearchOption';
const bgImage = require('../assets/images/bk1.png');
import Carousel, {Pagination} from 'react-native-snap-carousel';

const todayonly = require('../assets/images/todayonly.png');
const nearme = require('../assets/images/nearme.jpg');

import Loader from '../components/Loader';

class Home extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };
  currentCatPage = 1;
  categories = [];
  static contextType = NetworkContext;
  api = new ApiService();
  currentShop = null;
  state = {
    isSearch: false,
    loader: true,
    activeDotIndex: 1,
    isModalVisible: false,
    categoryLoader: true,
    catList: [],
    bottomLoader: false,
  };
  async componentDidMount() {
    this.userToken = await AsyncStorage.getItem('@token');
    this.userName = await AsyncStorage.getItem('@name');
    console.log(this.userName, 'home token');
    this.loadHome();
    this.requestLocationPermission();
  }

  componentWillUnmount() {
    this.watchID != null && Geolocation.clearWatch(this.watchID);
  }

  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'This App needs access to your location ' +
            'so we can know where you are.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use locations ');
        this.getCurrentPosition();
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  getCurrentPosition = () => {
    console.log('location access');
    this.watchID = Geolocation.getCurrentPosition(
      position => {
        const lat = parseFloat(position.coords.latitude);
        const long = parseFloat(position.coords.longitude);
        console.log('location', lat, long);
        this.setState({
          latitude: lat,
          longitude: long,
          ready: true,
        });
        // this.initCheckin();
      },
      error => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000},
    );
  };

  toggleModal = () => {
    console.log(this.currentShop, 'hi');
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  loadHome = () => {
    console.log('dddd');
    this.api
      .getHome()
      .then(res => {
        console.log('dddd', res);

        if (res.code === 400) {
        }
        if (res.code === 200) {
          this.offer = res.data.offers;
          this.today = res.data.today;
          this.loadCategories();
          this.setState({
            loader: false,
          });
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };
  loadCategories = (filter = '') => {
    this.api
      .getCategories(filter)
      .then(res => {
        if (res.code === 400) {
        }
        if (res.code === 200) {
          console.log(res.data);
          this.categories = res.data;

          this.maxPage = res.max_pages;

          const {catList} = this.state;
          let cat = catList.concat(res.data);
          // console.log(cat);
          this.setState({
            categoryLoader: false,
            catList: cat,
            bottomLoader: false,
          });
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };

  handleGetDirections = key => {
    const {latitude, longitude} = this.state;

    const data = {
      source: {
        latitude,
        longitude,
      },
      destination: {
        latitude: parseFloat(this.currentShop.location.latitude),
        longitude: parseFloat(this.currentShop.location.longitude),
      },
    };

    getDirections(data);
  };

  updateSearch = search => {
    this.setState({search});
  };

  _renderItem = ({item, index}) => {
    return (
      <Button
        onPress={() => {
          this.props.navigation.navigate('ListOffer', {category: item.id});
        }}
        style={{
          backgroundColor: 'white',
          height: wp('30%'),
          width: wp('30%'),
          borderRadius: 20,
          marginRight: wp('2%'),
          marginTop: hp('2%'),
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Icon name={item.icon} size={wp('5%')} color={Colors.APP.black} />
        <Text>{item.name}</Text>
      </Button>
    );
  };

  renderToday = ({item, index}) => {
    return (
      <Button
        onPress={() => {
          this.currentShop = item;
          this.toggleModal();
        }}
        style={{
          backgroundColor: 'white',
          borderRadius: 20,
          flexDirection: 'row',
          height: hp('20%'),
        }}>
        <Image
          source={{
            uri: `${MEDIA_URL}${item.banner}`,
          }}
          resizeMode="contain"
          style={{
            width: wp('45%'),
            height: wp('45%'),
            borderTopLeftRadius: 20,
            borderBottomLeftRadius: 20,
          }}
        />
        <Block
          style={{
            backgroundColor: 'white',
            width: wp('45%'),
            height: wp('45%'),
            borderBottomRightRadius: 20,
            borderTopRightRadius: 20,
          }}>
          <Block
            style={{
              position: 'absolute',
              paddingLeft: 5,
              bottom: 20,
            }}>
            <Text h5 bold>
              {item.title}
            </Text>
            <Text muted>{item.description}</Text>
          </Block>
          <Text
            color={Colors.APP.light}
            style={{
              backgroundColor: Colors.APP.base,
              padding: 10,
              borderRadius: 10,
              position: 'absolute',
              right: 20,
              top: 20,
              alignSelf: 'center',
              flex: 1,
            }}>
            {`${parseFloat(item.discount.toString())}% OFF`}
          </Text>
        </Block>
      </Button>
    );
  };

  renderCategoryCard = ({item, key}) => {
    return (
      <Button
        key={`cat-btn-${key}`}
        onPress={() => {
          this.props.navigation.navigate('ListOffer', {
            category: item.name,
          });
        }}
        style={{
          backgroundColor: 'white',
          height: wp('45%'),
          width: wp('45%'),
          borderRadius: 20,
          margin: 5,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Icon name={item.icon} size={wp('6%')} color={Colors.APP.black} />
        <Text bold>{item.name}</Text>
      </Button>
    );
  };

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  render() {
    const {
      categories,
      isSearch,
      activeDotIndex,
      loader,
      isModalVisible,
    } = this.state;
    const {navigation} = this.props;
    const {isConnected} = this.context;

    if (!isConnected) {
      return <Offline navigation={navigation} />;
    }
    if (isSearch) {
      return <SearchOption />;
    }
    return (
      <Block
        flex={1}
        style={{
          backgroundColor: Colors.APP.light,
        }}>
        <TopNavBar
          navigation={this.props.navigation}
          token={this.userToken}
          search={() => {
            this.setState({
              isSearch: true,
            });
          }}
        />
        {/* <Block
          center
          style={{
            backgroundColor: '#edece6',
            // alignItems: 'flex-start',
          }}> */}
        {/* <Button
            color="transparent"
            onPress={() => {
              navigation.navigate('Search');
            }}
            style={{
              backgroundColor: '#fff',
              width: wp('90%'),
              borderRadius: wp('5%'),
              borderWidth: 0,
              alignItems: 'flex-start',
              paddingLeft: wp('2%'),
              borderRadius: 10,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
            <Icon name="search" color={Colors.APP.base} size={wp('5%')} />
            <Text muted>
              {'  '}
              Search Shops, Categories or Locations...
            </Text>
          </Button> */}
        {/* </Block> */}
        {loader ? (
          <Loader />
        ) : (
          <ScrollView
            showsVerticalScrollIndicator={false}
            onScroll={({nativeEvent}) => {
              if (this.isCloseToBottom(nativeEvent)) {
                console.log(this.currentCatPage, this.maxPage);
                if (this.currentCatPage <= this.maxPage) {
                  this.setState({
                    bottomLoader: true,
                  });
                  this.currentCatPage = this.currentCatPage + 1;
                  this.loadCategories(`page=${this.currentCatPage}`);
                }
              }
            }}
            scrollEventThrottle={400}
            style={{
              paddingLeft: wp('3%'),
              // marginTop: hp('3%'),
              // backgroundColor: '#edece6',
            }}>
            <Block
              center
              style={{
                paddingVertical: hp('2%'),
              }}>
              <Text h5 uppercase>
                Explore the Hot City Coimbatore
              </Text>
              <Text p>How can we assist you?</Text>
            </Block>
            <Search navigation={this.props.navigation} />
            {this.today.length ? (
              <Block>
                <Text
                  p
                  style={{
                    marginBottom: hp('1%'),
                  }}>
                  Today's Offer
                </Text>

                <Carousel
                  ref={c => {
                    this.todayCarousel = c;
                  }}
                  style={{
                    alignSelf: 'center',
                  }}
                  data={this.today}
                  renderItem={this.renderToday}
                  sliderWidth={wp('90%')}
                  itemWidth={wp('90%')}
                  firstItem={3}
                  autoplay
                  loop
                  onSnapToItem={index => this.setState({activeDotIndex: index})}
                />
                <Pagination
                  dotsLength={this.today.length}
                  activeDotIndex={activeDotIndex}
                  // containerStyle={styles.paginationContainer}
                  dotColor={'rgba(255, 255, 255, 0.92)'}
                  // dotStyle={styles.paginationDot}
                  inactiveDotColor={Colors.APP.base}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                  carouselRef={this.todayCarousel}
                  tappableDots={!!this.todayCarousel}
                />
              </Block>
            ) : null}
            {this.offer.length ? (
              <Text
                color={Colors.APP.black}
                p
                style={{
                  marginBottom: hp('2%'),
                  zIndex: 100,
                }}>
                Trending Offers
              </Text>
            ) : null}

            <Block
              row
              style={{
                flexWrap: 'wrap',
                zIndex: 100,
              }}>
              {this.offer.map((item, key) => {
                return (
                  <Ripple
                    onPress={() => {
                      this.currentShop = item;
                      this.toggleModal();
                    }}
                    key={`trend-${key}`}
                    style={{
                      backgroundColor: 'white',
                      height: wp('60%'),
                      width: wp('45%'),
                      borderRadius: 20,
                      margin: wp('1%'),
                    }}>
                    <Image
                      source={{
                        uri: `${MEDIA_URL}${item.banner}`,
                      }}
                      resizeMode="contain"
                      style={{
                        width: wp('45%'),
                        height: wp('45%'),
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                      }}
                    />
                    <Block
                      row
                      style={{
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        padding: wp('1%'),
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                        paddingLeft: wp('2%'),
                      }}>
                      <Block style={{}}>
                        <Text>
                          {item.title.length > 23
                            ? `${item.title.substring(0, 23)}...`
                            : item.title}
                        </Text>
                        <Text muted>
                          {item.description.length > 20
                            ? `${item.description.substring(0, 20)}...`
                            : item.description}
                        </Text>
                      </Block>
                      {/* <Icon
                        name="arrow-right"
                        size={wp('5%')}
                        color={Colors.APP.black}
                      /> */}
                    </Block>
                  </Ripple>
                );
              })}
            </Block>
            {this.offer.length ? (
              <Button
                onPress={async () => {
                  this.userToken = await AsyncStorage.getItem('@token');

                  if (this.userToken) {
                    this.props.navigation.navigate('ListOffer', {
                      isToday: false,
                    });
                  } else {
                    this.props.navigation.navigate('SignIn');
                  }
                }}
                color={Colors.APP.base}
                style={{
                  marginTop: hp('3%'),
                  zIndex: 1,
                  position: 'relative',
                }}>
                <Text bold color={Colors.APP.white}>
                  View all offers
                </Text>
              </Button>
            ) : null}

            <Text
              color={Colors.APP.black}
              p
              style={{
                marginTop: hp('3%'),
                marginBottom: hp('1.5%'),
              }}>
              Featured Categories
            </Text>
            <Block
              row
              style={{
                flexWrap: 'wrap',
              }}>
              <FlatList
                data={this.state.catList}
                renderItem={this.renderCategoryCard}
                // keyExtractor={item => item.id}
                onEndReachedThreshold={0.1}
                numColumns={2}
                extraData={this.state}
                style={{
                  flexDirection: 'column',
                  alignSelf: 'center',
                  // marginLeft: wp('4%'),
                }}
                onEndReached={({distanceFromEnd}) => {}}
              />
              {/* {this.categories.map((item, key) => {
                // if (item.vendor_count <= 0) {
                //   return;
                // }
                return (
                  <Button
                    key={`cat-btn-${key}`}
                    onPress={() => {
                      this.props.navigation.navigate('ListOffer', {
                        category: item.name,
                      });
                    }}
                    style={{
                      backgroundColor: 'white',
                      height: wp('25%'),
                      width: wp('25%'),
                      borderRadius: 20,
                      margin: 5,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Icon
                      name={item.icon}
                      size={wp('5%')}
                      color={Colors.APP.black}
                    />
                    <Text
                      style={{
                        fontSize: item.name.length > 12 ? 8 : 12,
                      }}>
                      {item.name}
                    </Text>
                  </Button>
                );
              })} */}
            </Block>
            {this.state.bottomLoader ? (
              <Block
                style={{
                  height: hp('2%'),
                  // marginBottom: hp('5%'),
                }}>
                <Loader />
              </Block>
            ) : null}

            <Block style={{height: hp('4%')}} />
          </ScrollView>
        )}
        {isModalVisible ? (
          <Modal
            isVisible={isModalVisible}
            style={{
              justifyContent: 'flex-end',
              margin: 0,
              flex: 1,
              width: wp('100%'),
            }}>
            {!this.userToken ? (
              <Block
                style={{
                  flex: 0.3,
                  backgroundColor: Colors.APP.light,
                  width: wp('100%'),
                  // justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  h5
                  style={{
                    marginTop: hp('4%'),
                  }}>
                  Login to view more options
                </Text>
                <Button
                  onPress={() => {
                    this.props.navigation.navigate('SignIn');
                  }}
                  color="primary"
                  round
                  size="small"
                  style={{
                    borderWidth: 0,
                    borderColor: Colors.APP.light,
                    marginTop: hp('2%'),
                  }}>
                  <Text color={Colors.APP.white}>Login</Text>
                </Button>

                <Button
                  onPress={() => {
                    this.currentShop = null;
                    this.toggleModal();
                  }}
                  shadowless={false}
                  color="tranparent"
                  size="small"
                  style={{
                    borderWidth: 0,
                    borderColor: Colors.APP.light,
                    position: 'absolute',
                    bottom: 10,
                    elevation: 0,
                  }}>
                  <Text color={Colors.APP.black}>Back</Text>
                </Button>
              </Block>
            ) : (
              <Block
                style={{
                  flex: 0.7,
                  backgroundColor: Colors.APP.light,
                  width: wp('100%'),
                }}
                center>
                <Block
                  row
                  style={{
                    position: 'absolute',
                    top: -10,
                  }}>
                  <Block
                    style={{
                      height: 20,
                      width: 10,
                      borderTopRightRadius: 50,
                      borderBottomRightRadius: 50,
                      backgroundColor: Colors.APP.greyer,
                    }}
                  />
                  <Dash
                    style={{width: wp('96%'), height: 1, alignSelf: 'center'}}
                    dashColor={Colors.APP.greyer}
                  />
                  <Block
                    style={{
                      height: 20,
                      width: 10,
                      borderTopLeftRadius: 50,
                      borderBottomLeftRadius: 50,
                      backgroundColor: Colors.APP.greyer,
                    }}
                  />
                </Block>
                <Image
                  source={discount}
                  resizeMode="contain"
                  style={{
                    marginVertical: hp('1%'),
                    width: wp('15%'),
                    height: wp('15%'),
                  }}
                />
                <Text p bold>
                  {this.currentShop.vendor_shop_name}
                </Text>
                <Text>{this.currentShop.address}</Text>
                {this.currentShop.mobile ? (
                  <Text>{this.currentShop.mobile}</Text>
                ) : null}
                {this.currentShop.website ? (
                  <Text>{this.currentShop.website}</Text>
                ) : null}
                <Block row>
                  <Dash
                    style={{
                      width: wp('100%'),
                      height: 1,
                      marginVertical: hp('1%'),
                      alignSelf: 'center',
                    }}
                    dashColor={Colors.APP.white}
                  />
                </Block>
                <Text
                  color={Colors.APP.primary}
                  bold
                  style={{
                    backgroundColor: '#004B9385',
                    padding: wp('1%'),
                    width: wp('30%'),
                    borderRadius: 15,
                    marginTop: hp('1%'),
                    textAlign: 'center',
                    position: 'absolute',
                    right: 10,
                    top: 10,
                  }}>
                  {this.currentShop.category}
                </Text>
                <Text
                  style={{
                    backgroundColor: '#004B9385',
                    padding: wp('1%'),
                    width: wp('30%'),
                    borderRadius: 15,
                    marginTop: hp('1%'),
                    textAlign: 'center',
                    position: 'absolute',
                    left: 10,
                    top: 10,
                  }}
                  bold
                  color={Colors.APP.primary}>
                  {`Valid till ${this.currentShop.to_date}`}
                </Text>
                <Block
                  center
                  style={
                    {
                      // backgroundColor: 'yellow',
                    }
                  }>
                  <Text p>{this.currentShop.title}</Text>
                  <Text>{this.currentShop.description}</Text>
                  <Text
                    h4
                    bold
                    style={{
                      marginTop: hp('1%'),
                    }}>{`${parseFloat(
                    this.currentShop.discount.toString(),
                  )}% OFF`}</Text>
                  <Block row>
                    <Block
                      style={{
                        height: 20,
                        width: 10,
                        borderTopRightRadius: 50,
                        borderBottomRightRadius: 50,
                        backgroundColor: Colors.APP.greyer,
                      }}
                    />
                    <Dash
                      style={{
                        width: wp('96%'),
                        height: 1,
                        alignSelf: 'center',
                      }}
                      dashColor={Colors.APP.greyer}
                    />
                    <Block
                      style={{
                        height: 20,
                        width: 10,
                        borderTopLeftRadius: 50,
                        borderBottomLeftRadius: 50,
                        backgroundColor: Colors.APP.greyer,
                      }}
                    />
                  </Block>

                  <Block
                    style={{
                      marginVertical: hp('2%'),
                      width: wp('100%'),
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Button
                      round
                      onPress={() => {
                        this.currentShop = null;
                        this.toggleModal();
                        navigation.navigate('QuickPay');
                      }}
                      color="primary"
                      style={{
                        width: wp('80%'),
                        height: hp('4%'),
                        borderColor: Colors.APP.base,
                        marginVertical: 10,
                      }}>
                      <Text color={Colors.APP.white}>
                        Scan QR to avail offer
                      </Text>
                    </Button>

                    <Button
                      round
                      onPress={() => {
                        this.handleGetDirections();
                        // this.currentShop = null;
                        // this.toggleModal();
                      }}
                      color={Colors.APP.primary}
                      style={{
                        width: wp('80%'),
                        height: hp('4%'),
                        borderColor: Colors.APP.base,
                        padding: 5,
                      }}>
                      <Text color={Colors.APP.white}>
                        Get Direction on Gmaps
                      </Text>
                    </Button>
                  </Block>
                </Block>
                <Block
                  style={{
                    width: wp('100%'),
                    paddingHorizontal: wp('2%'),
                  }}>
                  {/* <Text>Other Offers in this Shop</Text> */}
                </Block>
                <Button
                  onPress={() => {
                    this.currentShop = null;
                    this.toggleModal();
                  }}
                  color="transparent"
                  size="small"
                  style={{
                    borderWidth: 0,
                    borderWidth: 1,
                    borderColor: Colors.APP.primary,
                    elevation: 0,
                    width: wp('20%'),
                    height: hp('4%'),
                    borderRadius: 20,
                  }}>
                  <Text color={Colors.APP.primary}>Back</Text>
                </Button>
              </Block>
            )}
          </Modal>
        ) : null}
      </Block>
    );
  }
}

export default Home;
