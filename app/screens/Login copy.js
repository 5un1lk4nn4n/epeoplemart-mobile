import React from 'react';
import {StyleSheet, KeyboardAvoidingView, StatusBar, Image} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {View} from 'react-native-animatable';
import AsyncStorage from '@react-native-community/async-storage';

// import Button from '../ui_components/button';
import {Block, Button, Input, Text, NavBar, Icon} from 'galio-framework';
import {login as goLogin} from '../services/api';
import {Colors, Typography} from '../styles';
import Toast from '../utils/Toast';
import {NetworkContext} from '../utils/NetworkProvider';
import ApiService from '../services/api';

const logo = require('../assets/images/logo.png');

// const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 40;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    ...Typography.ff.medium,
    justifyContent: 'space-around',
    backgroundColor: Colors.APP.light,
  },

  textbox: {
    width: wp('80%'),
    borderBottomColor: Colors.APP.primary,
    color: Colors.APP.primary,
    textAlign: 'center',
    margin: hp('3%'),
    ...Typography.fs.secondary,
    ...Typography.ff.medium,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginBottom: hp('2%'),
  },
  nextBtn: {
    marginTop: hp('4%'),
  },

  btnLabel: {
    color: Colors.APP.white,
    alignSelf: 'center',
    ...Typography.fs.primary,
    ...Typography.ff.medium,
  },
  infoMsg: {
    color: Colors.APP.greyer,
    ...Typography.fs.secondary,
    margin: hp('2%'),
    ...Typography.ff.light,
    alignSelf: 'center',
  },
});

export default class Login extends React.Component {
  static navigationOptions = {
    header: null,
  };

  api = new ApiService();

  static contextType = NetworkContext;

  message = null;

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      loading: false,
      mobile: null,
      name: null,
    };
  }

  showNext = () => {
    const {password, mobile} = this.state;
    const {isConnected} = this.context;
    if (!password || !mobile) {
      this.message = 'Both fields are required';
      this.setState(
        {
          visible: true,
        },
        () => {
          this.setState({
            visible: false,
          });
        },
      );

      return;
    }
    if (!isConnected) {
      this.message = 'You are offline';
      this.setState(
        {
          visible: true,
        },
        () => {
          this.setState({
            visible: false,
          });
        },
      );
      return;
    }
    this.setState({loading: true});
    this.api
      .login(password, mobile)
      .then(res => {
        if (res.code === 401) {
          this.message = res.message;
          this.setState(
            {
              visible: true,
            },
            () => {
              this.setState({
                visible: false,
              });
            },
          );
        }
        if (res.code === 200) {
          AsyncStorage.setItem('@token', res.data.token);
          AsyncStorage.setItem('@name', res.data.name);
          this.props.navigation.navigate('Home');
        }
        this.setState({loading: false});
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };

  login = data => {
    this.setState({loading: false});
    const {name, mobile} = this.state;
    if (data.code === 200) {
      this.showToast(data.message);
      const {navigation} = this.props;
      navigation.navigate('Otp', {name, mobile});
    } else if (data.code === 401 || data.code === 400) {
      //   showMessage({
      //     message: data.message,
      //     type: 'default',
      //     backgroundColor: Colors.APP.base, // background color
      //     color: Colors.APP.black,
      //   });
    }
  };

  hideToast = () => {
    this.setState({
      visible: false,
    });
  };

  showToast = msg => {
    this.message = msg;
    this.setState(
      {
        visible: true,
      },
      () => {
        this.hideToast();
      },
    );
  };

  handlePasswordChange = val => {
    this.setState({
      password: val,
    });
  };

  handleMobileChange = val => {
    this.setState({
      mobile: val,
    });
  };

  renderForm = () => {
    return (
      <Block style={{backgroundColor: Colors.APP.light}}>
        <Text>Login</Text>
        <Input
          placeholder="Your Mobile Number"
          type="number-pad"
          autoCapitalize="none"
          icon="mobile1"
          family="AntDesign"
          autoCorrect={false}
          ref={this.nameRef}
          returnKeyType="next"
          borderless
          placeholderTextColor={Colors.APP.black}
          onChangeText={val => this.handleMobileChange(val)}
          // onSubmitEditing={() => {
          //   if (this.nameRef.current) this.mobileRef.current.focus();
          // }}
          style={{
            width: wp('80%'),
            // borderBottomColor: Colors.APP.black,
            color: Colors.APP.black,
            textAlign: 'center',
            ...Typography.ff.bold,
            borderBottomWidth: 1,
            fontWeight: 'normal',
            // marginBottom: hp('2%'),
          }}
        />
        <Input
          type="default"
          secureTextEntry
          placeholder="Your Password"
          icon="lock"
          placeholderTextColor={Colors.APP.black}
          borderless
          family="AntDesign"
          autoCapitalize="none"
          returnKeyType="done"
          style={{
            width: wp('80%'),
            // borderBottomColor: Colors.APP.black,
            color: Colors.APP.black,
            textAlign: 'center',
            borderBottomWidth: 1,
          }}
          ref={this.mobileRef}
          maxLength={10}
          onSubmitEditing={() => this.showNext()}
          onChangeText={val => this.handlePasswordChange(val)}
        />
      </Block>
    );
  };

  render() {
    const {visible, loading} = this.state;

    return (
      <KeyboardAvoidingView style={styles.container} behavior="height" enabled>
        <Block flex={0.4}>
          <Image
            source={logo}
            resizeMode="contain"
            style={{
              width: wp('70%'),
              height: hp('6%'),
            }}
          />
        </Block>
        <Block flex={0.5} center>
          {this.renderForm()}
          <Button
            loading={loading}
            color={Colors.APP.base}
            style={styles.nextBtn}
            onPress={this.showNext}
            size="small"
            radius={15}>
            <Block row middle>
              <Text
                color={Colors.APP.white}
                style={{
                  ...Typography.ff.bold,
                  marginRight: wp('6%'),
                }}>
                Login
              </Text>
              <Icon
                name="arrow-right"
                size={wp('4%')}
                family="Feather"
                color={Colors.APP.white}
              />
            </Block>
          </Button>
        </Block>

        <Toast visible={visible} message={this.message} />
      </KeyboardAvoidingView>
    );
  }
}
