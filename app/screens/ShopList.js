import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  View,
  StyleSheet,
  PermissionsAndroid,
} from 'react-native';
import {MEDIA_URL} from '../constants';
import getDirections from 'react-native-google-maps-directions';
import {
  Autocomplete,
  withKeyboardAwareScrollView,
} from 'react-native-dropdown-autocomplete';
import Ripple from 'react-native-material-ripple';
import Dash from 'react-native-dash';

import {Block, Text, Input, Button} from 'galio-framework';
import Geolocation from 'react-native-geolocation-service';
// import {SearchBar} from 'react-native-elements';
import Modal from 'react-native-modal';
import {NetworkContext} from '../utils/NetworkProvider';
import Offline from '../components/Offline';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import MapView, {Marker} from 'react-native-maps';
import {Colors} from '../styles';
import TopNavBar from '../components/TopNavBar';
import Icon from 'react-native-vector-icons/Feather';

import SearchOption from '../components/SearchOption';
const bgImage = require('../assets/images/bk1.png');
import Carousel from 'react-native-snap-carousel';
import Loader from '../components/Loader';
import ApiService from '../services/api';

const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = 0.01;
const discount = require('../assets/images/discount.png');

const styles = StyleSheet.create({
  autocompletesContainer: {
    paddingTop: 0,
    zIndex: 1,
    width: '100%',
    paddingHorizontal: 8,
  },
  input: {maxHeight: 40},
  inputContainer: {
    display: 'flex',
    flexShrink: 0,
    flexGrow: 0,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#c7c6c1',
    paddingVertical: 13,
    paddingLeft: 12,
    paddingRight: '5%',
    width: '100%',
    justifyContent: 'flex-start',
  },
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  plus: {
    position: 'absolute',
    left: 15,
    top: 10,
  },
});

class ShopList extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };
  static contextType = NetworkContext;
  api = new ApiService();

  currentShop = null;
  currentPage = 1;
  state = {
    latitude: 0,
    longitude: 0,
    error: null,
    ready: false,
    activeShopName: null,
    activeCategory: 'All',
    isMapView: false,
    isModalVisible: false,
    loader: true,
    vendorList: [],
    bottomLoader: false,
    searchQuery: null,
    categories: [],
  };
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage

    this.requestLocationPermission();
    this.loadVendors();
  }

  loadVendors = filters => {
    this.api
      .getVendors(filters)
      .then(res => {
        console.log('over');
        if (res.code === 400) {
        }
        if (res.code === 200) {
          console.log(res.data);
          this.vendors = res.data;

          this.maxPage = res.max_pages;

          const {vendorList, searchQuery} = this.state;
          console.log(searchQuery, this.currentPage);
          if (searchQuery && this.currentPage === 1) {
            vendorList.length = 0;
          }
          //   ven = res.data;

          // console.log(cat);
          let ven = vendorList.concat(res.data);
          this.setState({
            loader: false,
            vendorList: ven,
            bottomLoader: false,
          });
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };

  renderItem(item) {
    return (
      <Button
        shadowless
        onPress={() => {
          const {activeCategory} = this.state;
          // if (activeCategory === item.name) {
          //   this.setState({
          //     activeCategory: null,
          //   });
          // } else {
          this.setState({
            activeCategory: item.name,
            searchQuery: item.name,
            categories: [],
          });
          // }
        }}
        style={{
          backgroundColor: Colors.APP.light,
          // height: wp('18%'),
          width: wp('90%'),
          padding: wp('1%'),
          // margin: 5,
          // borderRadius: 20,
          // flexDirection: 'row',
          // justifyContent: 'center',
          // alignItems: 'center',
          elevation: 0,
        }}>
        <Text
          color={Colors.APP.black}
          style={{
            // textWrap: 'wrap',
            textAlign: 'center',
            // fontSize: item.name.length > 10 ? 8 : 12,
          }}>
          {item.name}
        </Text>
      </Button>
    );
  }
  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  updateViews = vendorId => {
    this.api
      .vendorViewCount(vendorId)
      .then(res => {
        if (res.code === 400) {
        }
        if (res.code === 200) {
          this.loadOffers();
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };

  updateSearch = search => {
    this.setState({search});
  };

  _renderItem = ({item, index}) => {
    return (
      <Block
        style={{
          backgroundColor: 'white',
          height: wp('30%'),
          width: wp('30%'),
          borderRadius: 20,
          marginRight: wp('2%'),
          marginTop: hp('2%'),
        }}></Block>
    );
  };

  componentWillUnmount() {
    this.watchID != null && Geolocation.clearWatch(this.watchID);
  }

  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'This App needs access to your location ' +
            'so we can know where you are.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use locations ');
        this.getCurrentPosition();
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  setView = type => {
    this.setState({
      isMapView: type,
    });
  };

  getCurrentPosition = () => {
    console.log('location access');
    this.watchID = Geolocation.getCurrentPosition(
      position => {
        const lat = parseFloat(position.coords.latitude);
        const long = parseFloat(position.coords.longitude);
        console.log('location', lat, long);
        this.setState({
          latitude: lat,
          longitude: long,
          ready: true,
        });
        // this.initCheckin();
      },
      error => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000},
    );
  };

  handleGetDirections = (lat, long) => {
    const {latitude, longitude} = this.state;
    console.log(this.currentShop, 'ssd');
    const data = {
      source: {
        latitude,
        longitude,
      },
      destination: {
        latitude: parseFloat(lat),
        longitude: parseFloat(long),
      },
    };

    getDirections(data);
  };

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  filterShop = category => {
    var newList = DATA.filter(function(el) {
      return el.category === category.toLowerCase();
    });
    return newList;
  };

  onSearchLocation = value => {
    console.log(value);
    this.setState({
      searchQuery: value,
      // categories: [],
    });

    if (!value) {
      console.log('here', value);
      this.setState({
        categories: [],
      });
      return;
    } else {
      this.loadCategories(`search=${value}`);
    }
  };

  loadCategories = (filter = '') => {
    this.api
      .getCategories(filter)
      .then(res => {
        if (res.code === 400) {
        }
        if (res.code === 200) {
          if (this.state.searchQuery) {
            console.log(res.data);
            this.categories = res.data;
            this.setState({
              categories: res.data,
            });
          }
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };
  render() {
    const {
      ready,
      latitude,
      longitude,
      shopList,
      isMapView,
      isModalVisible,
      error,
      loader,
      submitLoader,
      successFlag,
      activeShopName,
      activeCategory,
      categories,
      searchQuery,
    } = this.state;
    const {navigation} = this.props;
    const {isConnected} = this.context;

    if (!isConnected) {
      return <Offline navigation={navigation} />;
    }

    if (loader) {
      return <Loader />;
    }
    return (
      <Block
        center
        // row
        flex={1}
        style={{
          // height: hp('30%'),
          width: wp('100%'),
          backgroundColor: Colors.APP.light,
        }}>
        <Block row center>
          <Input
            placeholder="Search shops or categories..."
            left
            icon="search"
            family="feather"
            onChangeText={this.onSearchLocation}
            value={searchQuery}
            iconSize={14}
            iconColor={Colors.APP.base}
            style={{
              width: wp('80%'),
              height: wp('10%'),
              borderTopRightRadius: 0,
              borderBottomRadiusRadius: 0,
              borderWidth: 0,
            }}
          />
          <Button
            color="transparent"
            onPress={() => {
              this.searchFilter = `search=${searchQuery}&is_mobile=${true}`;
              this.currentPage = 1;
              this.setState({
                categories: [],
              });
              this.loadVendors(this.searchFilter);
            }}
            style={{
              alignItems: 'center',
              backgroundColor: Colors.APP.primary,
              width: wp('10%'),
              borderWidth: 0,
              alignItems: 'flex-start',
              padding: wp('2%'),
              borderTopRightRadius: 10,
              borderBottomRightRadius: 10,
              borderTopLeftRadius: 0,
              borderBottomLeftRadius: 0,
            }}>
            <Icon name="arrow-right" color={Colors.APP.white} size={wp('5%')} />
          </Button>
        </Block>
        <Block
          // row
          center
          style={{
            backgroundColor: 'white',
            position: 'absolute',
            top: 50,
            zIndex: 1000,
            width: wp('80%'),
            // marginTop: 10,
          }}>
          {categories.length
            ? categories.map((item, key) => {
                return this.renderItem(item);
              })
            : null}
        </Block>

        <Block
          flex={1}
          style={{
            backgroundColor: Colors.APP.light,
          }}>
          <View style={{}}>
            {this.state.vendorList.length ? (
              <ScrollView
                onScroll={({nativeEvent}) => {
                  if (this.isCloseToBottom(nativeEvent)) {
                    if (this.currentPage <= this.maxPage) {
                      this.setState({
                        bottomLoader: true,
                      });
                      this.currentPage = this.currentPage + 1;
                      this.loadVendors(
                        `page=${this.currentPage}&${this.searchFilter}`,
                      );
                    }
                  }
                }}
                scrollEventThrottle={400}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}>
                {this.state.vendorList.map((shop, key) => {
                  return (
                    <Block
                      onPress={() => {
                        this.updateViews(shop.id);
                        // this.currentShop = shop;
                        // this.toggleModal();
                      }}
                      style={{
                        // flexDirection: 'row',
                        width: wp('90%'),
                        backgroundColor: Colors.APP.white,
                        marginBottom: hp('2%'),
                        justifyContent: 'flex-start',
                        borderRadius: 20,
                        paddingVertical: hp('1%'),
                      }}>
                      {/* <Image
                        source={{
                          uri: `${MEDIA_URL}${shop.banner}`,
                        }}
                        resizeMode="contain"
                        style={{
                          width: wp('30%'),
                          // backgroundColor: 'yellow',

                          height: wp('30%'),
                          // borderTopLeftRadius: 20,
                          // borderBottomLeftRadius: 20,
                        }}
                      /> */}
                      <Block
                        style={{
                          // backgroundColor: 'yellow',
                          // height: wp('30%'),
                          paddingVertical: hp('1%'),
                          marginLeft: wp('2%'),
                        }}>
                        <Block
                          row
                          style={{
                            // backgroundColor: Colors.APP.base,

                            padding: wp('2%'),
                            borderRadius: hp('2%'),
                            alignItems: 'center',
                          }}>
                          <Icon
                            name="tag"
                            size={wp('4%')}
                            color={Colors.APP.primary}
                            style={{marginRight: 10}}
                          />
                          <Text color={Colors.APP.primary}>
                            {shop.category.name}
                          </Text>
                        </Block>
                        <Block
                          style={{
                            // alignItems: 'center',
                            marginLeft: wp('1%'),
                            // marginBottom: wp('1%'),
                          }}>
                          <Text h5 bold>
                            {shop.company_name}
                          </Text>
                        </Block>
                        <Block
                          row
                          style={{
                            alignItems: 'center',
                            marginTop: hp('1%'),
                          }}>
                          <Icon
                            name="smartphone"
                            size={wp('4%')}
                            style={{marginRight: 10}}
                          />
                          <Text bold>
                            {shop.company_phone
                              ? `${shop.company_phone},${shop.mobile}`
                              : shop.mobile}
                          </Text>
                        </Block>
                        {shop.website ? (
                          <Block
                            row
                            style={{
                              alignItems: 'center',
                              marginTop: hp('1%'),
                            }}>
                            <Icon
                              name="globe"
                              size={wp('4%')}
                              style={{marginRight: 10}}
                            />
                            <Text bold>{shop.website}</Text>
                          </Block>
                        ) : null}
                        {shop.email ? (
                          <Block
                            row
                            style={{
                              alignItems: 'center',
                              marginTop: hp('1%'),
                            }}>
                            <Icon
                              name="mail"
                              size={wp('4%')}
                              style={{marginRight: 10}}
                            />
                            <Text bold>{shop.email}</Text>
                          </Block>
                        ) : null}

                        <Block
                          row
                          style={{
                            alignItems: 'center',
                            marginTop: hp('1%'),
                            flexWrap: 'wrap',
                          }}>
                          <Icon
                            name="map-pin"
                            size={wp('4%')}
                            style={{marginRight: 10}}
                          />
                          <Text bold style={{flex: 1, flexWrap: 'wrap'}}>
                            {shop.address}
                          </Text>
                        </Block>

                        <Block
                          row
                          style={{
                            alignItems: 'center',
                            marginTop: hp('1%'),
                          }}>
                          <Icon
                            name="eye"
                            size={wp('4%')}
                            style={{marginRight: 10}}
                          />
                          <Text
                            style={{
                              borderRadius: 15,
                              marginLeft: wp('1%'),
                            }}
                            bold
                            color={Colors.APP.black}>
                            {`${shop.views}`}
                          </Text>
                        </Block>
                      </Block>

                      <Block
                        row
                        style={{
                          marginVertical: hp('2%'),
                          width: wp('100%'),
                          // justifyContent: 'space-around',
                          // alignItems: 'center',
                          // backgroundColor: 'yellow',
                          marginLeft: wp('2%'),
                        }}>
                        <Button
                          round
                          onPress={() => {
                            this.updateViews(shop.id);
                            this.props.navigation.navigate('ListOffer', {
                              vendor_id: shop.id,
                            });
                          }}
                          color="primary"
                          style={{
                            width: wp('25%'),
                            height: hp('4%'),
                            borderColor: Colors.APP.base,
                            marginRight: 10,
                            padding: 5,
                          }}>
                          <Text color={Colors.APP.white}>View Offers</Text>
                        </Button>

                        <Button
                          round
                          onPress={() => {
                            this.updateViews(shop.id);

                            this.handleGetDirections(
                              shop.location.latitude,
                              shop.location.longitude,
                            );
                            // this.currentShop = null;
                            // this.toggleModal();
                          }}
                          color={Colors.APP.primary}
                          style={{
                            width: wp('25%'),
                            height: hp('4%'),
                            borderColor: Colors.APP.base,
                            padding: 5,
                          }}>
                          <Text color={Colors.APP.white}>Get Location</Text>
                        </Button>
                      </Block>
                    </Block>
                  );
                })}

                <Block
                  style={{
                    height: hp('6%'),
                    backgroundColor: Colors.APP.light,
                  }}></Block>
              </ScrollView>
            ) : (
              <Block>
                <Text muted p>
                  Nothing Found
                </Text>
              </Block>
            )}
          </View>
          {this.state.bottomLoader ? (
            <Block
              style={{
                height: hp('2%'),
                // marginBottom: hp('5%'),
              }}>
              <Loader />
            </Block>
          ) : null}
        </Block>
        {isModalVisible ? (
          <Modal
            isVisible={isModalVisible}
            style={{
              justifyContent: 'flex-end',
              margin: 0,
              flex: 1,
              width: wp('100%'),
            }}>
            <Block
              style={{
                flex: 0.7,
                backgroundColor: Colors.APP.light,
                width: wp('100%'),
              }}
              center>
              <Block
                row
                style={{
                  position: 'absolute',
                  top: -10,
                }}>
                <Block
                  style={{
                    height: 20,
                    width: 10,
                    borderTopRightRadius: 50,
                    borderBottomRightRadius: 50,
                    backgroundColor: Colors.APP.greyer,
                  }}
                />
                <Dash
                  style={{width: wp('96%'), height: 1, alignSelf: 'center'}}
                  dashColor={Colors.APP.greyer}
                />
                <Block
                  style={{
                    height: 20,
                    width: 10,
                    borderTopLeftRadius: 50,
                    borderBottomLeftRadius: 50,
                    backgroundColor: Colors.APP.greyer,
                  }}
                />
              </Block>
              <Image
                source={discount}
                resizeMode="contain"
                style={{
                  marginVertical: hp('1%'),
                  width: wp('15%'),
                  height: wp('15%'),
                }}
              />
              <Text p bold>
                {this.currentShop.vendor_shop_name}
              </Text>
              <Text>{this.currentShop.address}</Text>
              <Block row>
                <Dash
                  style={{
                    width: wp('100%'),
                    height: 1,
                    marginVertical: hp('1%'),
                    alignSelf: 'center',
                  }}
                  dashColor={Colors.APP.white}
                />
              </Block>
              <Text
                color={Colors.APP.primary}
                bold
                style={{
                  backgroundColor: '#004B9385',
                  padding: wp('1%'),
                  width: wp('30%'),
                  borderRadius: 15,
                  marginTop: hp('1%'),
                  textAlign: 'center',
                  position: 'absolute',
                  right: 10,
                  top: 10,
                }}>
                {this.currentShop.category}
              </Text>
              <Text
                style={{
                  backgroundColor: '#004B9385',
                  padding: wp('1%'),
                  width: wp('30%'),
                  borderRadius: 15,
                  marginTop: hp('1%'),
                  textAlign: 'center',
                  position: 'absolute',
                  left: 10,
                  top: 10,
                }}
                bold
                color={Colors.APP.primary}>
                {`Valid till ${this.currentShop.to_date}`}
              </Text>
              <Block
                center
                style={
                  {
                    // backgroundColor: 'yellow',
                  }
                }>
                <Text p>{this.currentShop.title}</Text>
                <Text>{this.currentShop.description}</Text>
                <Text
                  h4
                  bold
                  style={{
                    marginTop: hp('1%'),
                  }}>{`${parseFloat(
                  this.currentShop.discount.toString(),
                )}% OFF`}</Text>
                <Block row>
                  <Block
                    style={{
                      height: 20,
                      width: 10,
                      borderTopRightRadius: 50,
                      borderBottomRightRadius: 50,
                      backgroundColor: Colors.APP.greyer,
                    }}
                  />
                  <Dash
                    style={{width: wp('96%'), height: 1, alignSelf: 'center'}}
                    dashColor={Colors.APP.greyer}
                  />
                  <Block
                    style={{
                      height: 20,
                      width: 10,
                      borderTopLeftRadius: 50,
                      borderBottomLeftRadius: 50,
                      backgroundColor: Colors.APP.greyer,
                    }}
                  />
                </Block>

                <Block
                  style={{
                    marginVertical: hp('2%'),
                    width: wp('100%'),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Button
                    round
                    onPress={() => {
                      this.currentShop = null;
                      this.toggleModal();
                      navigation.navigate('QuickPay');
                    }}
                    color="primary"
                    style={{
                      width: wp('80%'),
                      height: hp('4%'),
                      borderColor: Colors.APP.base,
                      marginVertical: 10,
                    }}>
                    <Text color={Colors.APP.white}>Scan QR to avail offer</Text>
                  </Button>

                  <Button
                    round
                    onPress={() => {
                      this.handleGetDirections();
                      // this.currentShop = null;
                      // this.toggleModal();
                    }}
                    color={Colors.APP.primary}
                    style={{
                      width: wp('80%'),
                      height: hp('4%'),
                      borderColor: Colors.APP.base,
                      padding: 5,
                    }}>
                    <Text color={Colors.APP.white}>Get Direction on Gmaps</Text>
                  </Button>
                </Block>
              </Block>
              <Block
                style={{
                  width: wp('100%'),
                  paddingHorizontal: wp('2%'),
                }}>
                {/* <Text>Other Offers in this Shop</Text> */}
              </Block>
              <Button
                onPress={() => {
                  this.currentShop = null;
                  this.toggleModal();
                }}
                color="transparent"
                size="small"
                style={{
                  borderWidth: 0,
                  borderWidth: 1,
                  borderColor: Colors.APP.primary,
                  elevation: 0,
                  width: wp('20%'),
                  height: hp('4%'),
                  borderRadius: 20,
                }}>
                <Text color={Colors.APP.primary}>Back</Text>
              </Button>
            </Block>
          </Modal>
        ) : null}
      </Block>
    );
  }
}

export default ShopList;
