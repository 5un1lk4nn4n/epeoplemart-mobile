import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  View,
  StyleSheet,
  PermissionsAndroid,
} from 'react-native';

import Ripple from 'react-native-material-ripple';
import Dash from 'react-native-dash';

import {Block, Text, Input, Button} from 'galio-framework';
import Geolocation from 'react-native-geolocation-service';
// import {SearchBar} from 'react-native-elements';
import Modal from 'react-native-modal';
import {NetworkContext} from '../utils/NetworkProvider';
import Offline from '../components/Offline';
import getDirections from 'react-native-google-maps-directions';
import {MEDIA_URL} from '../constants';
import Loader from '../components/Loader';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ApiService from '../services/api';

import {Colors} from '../styles';
import Icon from 'react-native-vector-icons/Feather';

const discount = require('../assets/images/discount.png');

const CATEGORIES = ['Restautent', 'Supermarket', 'Mobile', 'Clothing'];

class NearMe extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };
  static contextType = NetworkContext;
  api = new ApiService();
  currentShop = null;
  categories = [];

  state = {
    latitude: 0,
    longitude: 0,
    error: null,
    ready: false,
    activeShopName: null,
    activeCategory: 'All',
    isMapView: false,
    isModalVisible: false,
    loader: true,
  };
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage

    this.requestLocationPermission();
  }

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  updateSearch = search => {
    this.setState({search});
  };

  handleGetDirections = key => {
    const des = [
      {
        latitude: 10.9787,
        longitude: 77.0242,
      },
      {
        latitude: 10.9787,
        longitude: 77.0242,
      },
      {
        latitude: 11.002371,
        longitude: 77.029089,
      },
      {
        latitude: 11.000233,
        longitude: 77.026056,
      },
      {
        latitude: 11.001571,
        longitude: 77.025134,
      },
    ];
    const data = {
      source: {
        latitude: 11.0104,
        longitude: 76.9499,
      },
      destination: des[Math.floor(Math.random() * Math.floor(4))],
      params: [
        {
          key: 'travelmode',
          value: 'driving', // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: 'dir_action',
          value: 'navigate', // this instantly initializes navigation using the given travel mode
        },
      ],
    };

    getDirections(data);
  };

  _renderItem = ({item, index}) => {
    return (
      <Block
        style={{
          backgroundColor: 'white',
          height: wp('30%'),
          width: wp('30%'),
          borderRadius: 20,
          marginRight: wp('2%'),
          marginTop: hp('2%'),
        }}></Block>
    );
  };

  componentWillUnmount() {
    this.watchID != null && Geolocation.clearWatch(this.watchID);
  }

  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'This App needs access to your location ' +
            'so we can know where you are.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use locations ');
        this.getCurrentPosition();
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  setView = type => {
    this.setState({
      isMapView: type,
    });
  };

  getCurrentPosition = () => {
    console.log('location access');
    this.watchID = Geolocation.getCurrentPosition(
      position => {
        const lat = parseFloat(position.coords.latitude);
        const long = parseFloat(position.coords.longitude);
        console.log('location', lat, long);
        this.setState({
          latitude: lat,
          longitude: long,
          ready: true,
        });
        this.initCheckin();
      },
      error => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000},
    );
  };
  initCheckin = () => {
    const {latitude, longitude} = this.state;

    this.api
      .nearMe({latitude, longitude})
      .then(res => {
        console.log('dddd', res);

        if (res.code === 400) {
        }
        if (res.code === 200) {
          this.nearMeStores = res.data;
          this.setState({
            loader: false,
          });
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };
  filterShop = category => {
    var newList = DATA.filter(function(el) {
      return el.category === category.toLowerCase();
    });
    return newList;
  };

  updateViews = vendorId => {
    this.api
      .vendorViewCount(vendorId)
      .then(res => {
        if (res.code === 400) {
        }
        if (res.code === 200) {
          this.initCheckin();
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };
  render() {
    const {categories, isSearch} = this.state;
    const {
      ready,
      latitude,
      longitude,
      shopList,
      isMapView,
      isModalVisible,
      error,
      loader,
      submitLoader,
      successFlag,
      activeShopName,
      activeCategory,
    } = this.state;
    const {navigation} = this.props;
    const {isConnected} = this.context;
    if (!isConnected) {
      return <Offline navigation={navigation} />;
    }

    if (loader) {
      return <Loader />;
    }
    if (!this.nearMeStores.length) {
      return (
        <Block
          flex={1}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text p muted>
            Sorry, No Stores nearby
          </Text>
        </Block>
      );
    }
    return (
      <Block
        center
        flex={1}
        style={{
          // height: hp('30%'),
          paddingTop: hp('1%'),
          width: wp('100%'),
          backgroundColor: Colors.APP.light,
        }}>
        <Button
          color="transparent"
          onPress={() => {
            navigation.navigate('Search');
          }}
          style={{
            backgroundColor: '#fff',
            width: wp('90%'),
            borderRadius: wp('5%'),
            borderWidth: 0,
            alignItems: 'flex-start',
            paddingHorizontal: wp('2%'),
            borderRadius: 10,
            marginTop: hp('2%'),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Icon name="map-pin" color={Colors.APP.base} size={wp('5%')} />
          <Text muted>{` ${latitude}, ${longitude}`}</Text>
          <Icon name="search" color={Colors.APP.base} size={wp('5%')} />
        </Button>

        <Block
          row
          center
          style={{
            width: wp('90%'),
            marginTop: 10,
            backgroundColor: Colors.APP.light,
          }}>
          <ScrollView
            horizontal
            style={
              {
                // marginVertical: hp('1%'),
              }
            }>
            {/* <Button
              color="transparent"
              onPress={() => {
                this.setState({
                  activeCategory: null,
                  shopList: DATA,
                });
              }}
              style={{
                borderColor: Colors.APP.primary,
                width: wp('8%'),
                height: hp('3%'),
                borderRadius: hp('2.5%'),
                margin: 5,
              }}>
              <Text color={Colors.APP.primary}>All</Text>
            </Button> */}
            {this.categories.map((cat, key) => {
              return (
                <Button
                  color="transparent"
                  onPress={() => {
                    this.setState({
                      activeCategory: cat,
                      shopList: this.filterShop(cat),
                    });
                  }}
                  style={{
                    borderColor: Colors.APP.primary,
                    width: wp('20%'),
                    height: hp('3%'),
                    borderRadius: hp('2.5%'),
                    margin: 5,
                  }}>
                  <Text color={Colors.APP.primary}>{cat}</Text>
                </Button>
              );
            })}
          </ScrollView>
        </Block>

        <Block
          flex={1}
          style={{
            backgroundColor: Colors.APP.light,
          }}>
          <View style={{}}>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}>
              {this.nearMeStores.map((shop, key) => {
                // this.categories.push(shop.category);

                return shop.offer.map((offer, key) => {
                  return (
                    <Ripple
                      onPress={() => {
                        console.log(offer);
                        this.updateViews(shop.id);
                        this.currentShop = offer;
                        this.toggleModal();
                      }}
                      style={{
                        // padding: 5,
                        flexDirection: 'row',
                        width: wp('90%'),
                        backgroundColor: Colors.APP.white,
                        marginBottom: hp('2%'),
                        justifyContent: 'flex-start',
                        paddingVertical: hp('1%'),
                      }}>
                      <Image
                        source={{
                          uri: `${MEDIA_URL}${offer.banner}`,
                        }}
                        resizeMode="contain"
                        style={{
                          width: wp('30%'),
                          height: wp('30%'),
                        }}
                      />
                      <Block
                        style={{
                          // backgroundColor: 'yellow',
                          height: wp('30%'),
                          paddingVertical: hp('1%'),
                          marginLeft: wp('2%'),
                        }}>
                        <Text p bold>
                          {offer.title}
                        </Text>
                        <Text muted>{offer.description}</Text>
                        <Block
                          row
                          style={{
                            alignItems: 'center',

                            // justifyContent: 'space-around',
                          }}>
                          <Text
                            h5
                            style={{
                              marginRight: wp('1%'),
                            }}>
                            {`${parseFloat(offer.discount.toString())}% OFF`}
                          </Text>
                          <Text>{'discount'}</Text>
                        </Block>
                        <Text
                          style={{
                            // padding: wp('1%'),
                            borderRadius: 15,
                            marginTop: hp('1%'),
                          }}
                          bold
                          color={Colors.APP.primary}>
                          {`Valid till ${offer.to_date}`}
                        </Text>
                        <Block
                          row
                          style={{
                            alignItems: 'center',
                          }}>
                          <Icon name="eye" size={wp('3%')} style={{}} />
                          <Text
                            style={{
                              // backgroundColor: '#004B9385',
                              // padding: wp('1%'),
                              borderRadius: 15,
                              marginLeft: wp('1%'),
                            }}
                            bold
                            color={Colors.APP.black}>
                            {`${shop.views}`}
                          </Text>
                        </Block>
                      </Block>

                      <Icon
                        name="arrow-right"
                        size={wp('5%')}
                        style={{
                          position: 'absolute',
                          right: 10,
                          top: 10,
                        }}
                      />
                    </Ripple>
                  );
                });
              })}

              <Block
                style={{
                  height: hp('6%'),
                  backgroundColor: Colors.APP.light,
                }}></Block>
            </ScrollView>
          </View>
        </Block>

        {isModalVisible ? (
          <Modal
            isVisible={isModalVisible}
            style={{
              justifyContent: 'flex-end',
              margin: 0,
              flex: 1,
              width: wp('100%'),
            }}>
            <Block
              style={{
                flex: 0.7,
                backgroundColor: Colors.APP.light,
                width: wp('100%'),
              }}
              center>
              <Block
                row
                style={{
                  position: 'absolute',
                  top: -10,
                }}>
                <Block
                  style={{
                    height: 20,
                    width: 10,
                    borderTopRightRadius: 50,
                    borderBottomRightRadius: 50,
                    backgroundColor: Colors.APP.greyer,
                  }}
                />
                <Dash
                  style={{width: wp('96%'), height: 1, alignSelf: 'center'}}
                  dashColor={Colors.APP.greyer}
                />
                <Block
                  style={{
                    height: 20,
                    width: 10,
                    borderTopLeftRadius: 50,
                    borderBottomLeftRadius: 50,
                    backgroundColor: Colors.APP.greyer,
                  }}
                />
              </Block>
              <Image
                source={discount}
                resizeMode="contain"
                style={{
                  marginVertical: hp('1%'),
                  width: wp('15%'),
                  height: wp('15%'),
                }}
              />
              <Text p bold>
                {this.currentShop.vendor_shop_name}
              </Text>
              <Text>{this.currentShop.address}</Text>
              <Block row>
                <Dash
                  style={{
                    width: wp('100%'),
                    height: 1,
                    marginVertical: hp('1%'),
                    alignSelf: 'center',
                  }}
                  dashColor={Colors.APP.white}
                />
              </Block>
              <Text
                color={Colors.APP.primary}
                bold
                style={{
                  backgroundColor: '#004B9385',
                  padding: wp('1%'),
                  width: wp('30%'),
                  borderRadius: 15,
                  marginTop: hp('1%'),
                  textAlign: 'center',
                  position: 'absolute',
                  right: 10,
                  top: 10,
                }}>
                {this.currentShop.category}
              </Text>
              <Text
                style={{
                  backgroundColor: '#004B9385',
                  padding: wp('1%'),
                  width: wp('30%'),
                  borderRadius: 15,
                  marginTop: hp('1%'),
                  textAlign: 'center',
                  position: 'absolute',
                  left: 10,
                  top: 10,
                }}
                bold
                color={Colors.APP.primary}>
                {`Valid till ${this.currentShop.to_date}`}
              </Text>
              <Block
                center
                style={
                  {
                    // backgroundColor: 'yellow',
                  }
                }>
                <Text p>{this.currentShop.title}</Text>
                <Text>{this.currentShop.description}</Text>
                <Text
                  h4
                  bold
                  style={{
                    marginTop: hp('1%'),
                  }}>{`${parseFloat(
                  this.currentShop.discount.toString(),
                )}% OFF`}</Text>
                <Block row>
                  <Block
                    style={{
                      height: 20,
                      width: 10,
                      borderTopRightRadius: 50,
                      borderBottomRightRadius: 50,
                      backgroundColor: Colors.APP.greyer,
                    }}
                  />
                  <Dash
                    style={{width: wp('96%'), height: 1, alignSelf: 'center'}}
                    dashColor={Colors.APP.greyer}
                  />
                  <Block
                    style={{
                      height: 20,
                      width: 10,
                      borderTopLeftRadius: 50,
                      borderBottomLeftRadius: 50,
                      backgroundColor: Colors.APP.greyer,
                    }}
                  />
                </Block>

                <Block
                  style={{
                    marginVertical: hp('2%'),
                    width: wp('100%'),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Button
                    round
                    onPress={() => {
                      this.currentShop = null;
                      this.toggleModal();
                      navigation.navigate('QuickPay');
                    }}
                    color="primary"
                    style={{
                      width: wp('80%'),
                      height: hp('4%'),
                      borderColor: Colors.APP.base,
                      marginVertical: 10,
                    }}>
                    <Text color={Colors.APP.white}>Scan QR to avail offer</Text>
                  </Button>

                  <Button
                    round
                    onPress={() => {
                      this.handleGetDirections();
                      // this.currentShop = null;
                      // this.toggleModal();
                    }}
                    color={Colors.APP.primary}
                    style={{
                      width: wp('80%'),
                      height: hp('4%'),
                      borderColor: Colors.APP.base,
                      padding: 5,
                    }}>
                    <Text color={Colors.APP.white}>Get Direction on Gmaps</Text>
                  </Button>
                </Block>
              </Block>
              <Block
                style={{
                  width: wp('100%'),
                  paddingHorizontal: wp('2%'),
                }}>
                {/* <Text>Other Offers in this Shop</Text> */}
              </Block>
              <Button
                onPress={() => {
                  this.currentShop = null;
                  this.toggleModal();
                }}
                color="transparent"
                size="small"
                style={{
                  borderWidth: 0,
                  borderWidth: 1,
                  borderColor: Colors.APP.primary,
                  elevation: 0,
                  width: wp('20%'),
                  height: hp('4%'),
                  borderRadius: 20,
                }}>
                <Text color={Colors.APP.primary}>Back</Text>
              </Button>
            </Block>
          </Modal>
        ) : null}
      </Block>
    );
  }
}

export default NearMe;
