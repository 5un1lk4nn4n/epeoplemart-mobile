import React from 'react';
import {View} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  bootstrapAsync = async () => {
    const {navigation} = this.props;
    const userToken = await AsyncStorage.getItem('@token');

    global.userToken = userToken;

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    navigation.navigate(userToken ? 'App' : 'App');
  };

  // Render any loading content that you like here
  render() {
    return <View />;
  }
}
