import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {Block, Text, Input} from 'galio-framework';
// import {SearchBar} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Colors} from '../styles';
import Icon from 'react-native-vector-icons/Feather';

const CONTENT = [
  {
    title: 'Saravan Stores',
    date: '21-11-2019',
    amount: '1000000',
    order_id: '122DDDF445',
  },
  {
    title: 'Chennai Silks',
    date: '21-11-2019',
    amount: '1000',
    order_id: '122DDDF445',
  },
  {
    title: 'RMKV Silks',
    date: '21-12-2019',
    amount: '1000',
    order_id: '122DDDF445',
  },
  {
    title: 'RMKV Silks',
    date: '21-12-2019',
    amount: '1000',
    order_id: '122DDDF445',
  },
  {
    title: 'RMKV Silks',
    date: '21-12-2019',
    amount: '1000',
    order_id: '122DDDF445',
  },
];

class History extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };

  previousDate = null;

  constructor(props) {
    super(props);
    // StatusBar.setBackgroundColor(Colors.APP.secondary, true);

    // StatusBar.setBarStyle('dark-content', true);
    this.state = {
      collapsed: true,
      activeSections: [],
      visible: false,
      multipleSelect: false,
    };
  }

  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
  }

  render() {
    const {visible, multipleSelect, activeSections} = this.state;
    return (
      <Block
        flex={1}
        style={{
          backgroundColor: Colors.APP.light,
          alignItems: 'flex-start',
        }}>
        <Text
          bold
          h3
          style={{
            paddingLeft: wp('3%'),
            marginVertical: hp('3%'),
          }}>
          Purchase History
        </Text>

        {CONTENT.map((item, key) => {
          return (
            <Block
              key={key}
              space="around"
              width={wp('100%')}
              style={{
                backgroundColor: Colors.APP.white,
                marginVertical: hp('1%'),
                height: 100,
                paddingLeft: wp('3%'),
              }}>
              <Block
                row
                style={{alignItems: 'center'}}
                space="between"
                width={wp('95%')}>
                <Text p>{item.title}</Text>
                <Text>{item.date}</Text>
              </Block>
              <Block
                row
                style={{
                  alignItems: 'center',
                }}>
                <Text
                  color={Colors.APP.white}
                  style={{
                    backgroundColor: Colors.APP.primary,
                    padding: wp('1.5%'),
                    borderRadius: 25,
                    marginRight: wp('3%'),
                  }}>
                  {`\u20B9 ${item.amount}`}
                </Text>
                <Block
                  style={{
                    borderLeftWidth: 1,
                    borderLeftColor: Colors.APP.black,
                    height: hp('3%'),
                    marginRight: wp('3%'),
                  }}
                />
                <Text>{`ORDER ID ${item.order_id}`}</Text>
              </Block>
            </Block>
          );
        })}
      </Block>
    );
  }
}

export default History;
