import React, {Component} from 'react';
import {Image, ImageBackground} from 'react-native';
import {Block, Text, Input, Button} from 'galio-framework';
// import {SearchBar} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AlphaScrollFlatList from 'alpha-scroll-flat-list';
import {Colors} from '../styles';
import TopNavBar from '../components/TopNavBar';
import Icon from 'react-native-vector-icons/Feather';
import {ScrollView} from 'react-native-gesture-handler';
import {NetworkContext} from '../utils/NetworkProvider';
import ApiService from '../services/api';
import Loader from '../components/Loader';

import Offline from '../components/Offline';
const ITEM_HEIGHT = 50;
const bgImage = require('../assets/images/bk1.png');
import Carousel from 'react-native-snap-carousel';
import NearMe from './NearMe';
const offer1 = require('../assets/images/1.jpg');
const offer2 = require('../assets/images/2.jpg');
const offer3 = require('../assets/images/3.jpg');
const offer4 = require('../assets/images/4.jpg');
const offer5 = require('../assets/images/5.jpg');
const offer6 = require('../assets/images/6.jpg');
const fruits = require('../assets/images/fruit.jpg');
const icecream = require('../assets/images/icecream.jpg');
const cloth = require('../assets/images/cloth.jpg');
const headphone = require('../assets/images/headphone.jpg');
const dayoffer = require('../assets/images/dayoffer.jpg');
const CATEGORIES = [
  'One Day Offer',
  'Supermarket',
  'Mobile',
  'Clothing',
  'Weekend Offer',
];
const styles = {};
const choosenCategories = [];
class Search extends Component {
  static navigationOptions = {
    headerShown: false,
  };
  static contextType = NetworkContext;
  api = new ApiService();
  locationSuggestion = [];
  categories = [];
  state = {
    isSearch: false,
    loader: true,
    searchQuery: null,
    categoryLoader: true,
    isNearMe: false,
    isTodayOffer: false,
    choosenCategories: [],
    locationSuggestion: [],
    locationQuery: null,
    categories: [],
  };
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    this.loadSuggetion();
  }

  loadSuggetion = () => {
    this.api
      .autoSuggestion()
      .then(res => {
        console.log('sug', res);
        if (res.code === 400) {
        }
        if (res.code === 200) {
          this.suggestion = res.data;
          this.setState({
            loader: false,
          });
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };
  loadCategories = (filter = '') => {
    this.api
      .getCategories(filter)
      .then(res => {
        if (res.code === 400) {
        }
        if (res.code === 200) {
          console.log(res.data);
          this.categories = res.data;
          this.setState({
            categories: res.data,
          });
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };

  renderCategoryScroll = () => {
    return this.categories.map((item, key) => {
      return this.renderItem(item);
    });
  };

  renderItem(item) {
    return (
      <Button
        shadowless
        onPress={() => {
          const {activeCategory} = this.state;
          // if (activeCategory === item.name) {
          //   this.setState({
          //     activeCategory: null,
          //   });
          // } else {
          this.setState({
            activeCategory: item.name,
            searchQuery: item.name,
            categories: [],
          });
          // }
        }}
        style={{
          backgroundColor: 'white',
          // height: wp('18%'),
          width: wp('90%'),
          padding: wp('1%'),
          // margin: 5,
          // borderRadius: 20,
          // flexDirection: 'row',
          // justifyContent: 'center',
          // alignItems: 'center',
          elevation: 0,
        }}>
        <Text
          color={Colors.APP.black}
          style={{
            // textWrap: 'wrap',
            textAlign: 'center',
            // fontSize: item.name.length > 10 ? 8 : 12,
          }}>
          {item.name}
        </Text>
      </Button>
    );
  }

  keyExtractor(item) {
    return item.id;
  }

  onChangeValue = value => {
    this.setState({
      searchQuery: value,
    });

    if (!value) {
      this.setState({
        categories: [],
      });
      return;
    } else {
      this.loadCategories(`search=${value}`);
    }
  };

  onSearchLocation = value => {
    this.setState({
      locationQuery: value,
    });

    if (!value) {
      this.setState({
        locationSuggestion: [],
      });
      return;
    }

    this.api
      .autoSuggestion({
        location: value,
      })
      .then(res => {
        console.log('sug', res);
        if (res.code === 400) {
        }
        if (res.code === 200) {
          this.setState({
            locationSuggestion: res.data.location,
          });
        }
      })
      .catch(error => {
        console.log('error');
        console.log(error);
      });
  };

  render() {
    const {
      categoryLoader,
      categories,
      choosenCategories,
      loader,
      searchQuery,
      isTodayOffer,
      isNearMe,
      activeCategory,
      activeLocation,
      locationQuery,
      locationSuggestion,
    } = this.state;
    console.log(locationSuggestion);
    const {navigation} = this.props;
    const {isConnected} = this.context;
    if (!isConnected) {
      return <Offline navigation={navigation} />;
    }
    return (
      <Block
        // flex={1}
        style={{
          backgroundColor: Colors.APP.light,
          paddingRight: wp('4%'),
          zIndex: 1000,
        }}>
        {/* <Block
          row
          style={{
            alignItems: 'center',
            padding: wp('2%'),
          }}>
          <Button
            color="transparent"
            onPress={() => {
              navigation.goBack();
            }}
            style={{
              alignItems: 'center',
              // backgroundColor: Colors.APP.primary,
              width: wp('10%'),
              height: wp('10%'),
              // borderRadius: wp('5%'),
              borderWidth: 0,
              alignItems: 'flex-start',
              padding: wp('2%'),
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
            }}>
            <Icon name="arrow-left" color={Colors.APP.black} size={wp('5%')} />
          </Button>
          <Text>Search for Shops, Categories, Locations and Offers </Text>
        </Block> */}

        <Block
          row
          style={{
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <Button
            color="transparent"
            onPress={() => {
              // navigation.goBack();
            }}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors.APP.primary,
              width: wp('10%'),
              height: wp('10%'),
              borderWidth: 0,
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
            }}>
            <Icon name="map-pin" color={Colors.APP.white} size={wp('4%')} />
          </Button>

          <Input
            placeholder="Search Locations..."
            left
            icon="search"
            family="feather"
            onChangeText={this.onSearchLocation}
            value={locationQuery}
            iconSize={14}
            iconColor={Colors.APP.base}
            style={{
              width: wp('80%'),
              height: wp('10%'),
              borderTopLeftRadius: 0,
              borderBottomLeftRadius: 0,
              borderWidth: 0,
            }}
          />
          {/* <Button
            color={Colors.APP.base}
            onPress={() => {
              const filters = {
                search: searchQuery,
                isToday: isTodayOffer,
                category: choosenCategories[0] || activeCategory,
                location: activeLocation,
              };

              if (isNearMe) {
                this.props.navigation.navigate('NearMe', filters);
              } else {
                this.props.navigation.navigate('ListOffer', filters);
              }
            }}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContents: 'center',
              width: wp('10%'),
              height: wp('10%'),
              // borderRadius: wp('5%'),
              borderWidth: 0,
              padding: wp('2%'),

              borderTopRightRadius: 10,
              borderBottomRightRadius: 10,
              borderTopLeftRadius: 0,
              borderBottomLeftRadius: 0,
            }}>
            <Icon name="search" color={Colors.APP.white} size={wp('5%')} />
          </Button> */}
        </Block>
        <Block
          center
          style={{
            // flexWrap: 'wrap',
            backgroundColor: Colors.APP.white,
            width: wp('90%'),
            zIndex: 1000,
            position: 'absolute',
            top: 50,
          }}>
          {locationSuggestion.length
            ? locationSuggestion.map((item, key) => {
                return (
                  <Button
                    key={`lbtn-${key}`}
                    color={Colors.APP.white}
                    shadowless
                    onPress={() => {
                      this.setState({
                        locationQuery: item.address_line_2,
                        locationSuggestion: [],
                      });
                    }}
                    style={{
                      width: wp('90%'),
                      height: hp('3%'),
                      borderRadius: hp('2.5%'),
                      margin: 5,
                      padding: 5,
                      elevation: 0,
                    }}>
                    <Text
                      // size={item.address_line_2.length > 10 ? 11 : 14}
                      color={Colors.APP.black}>
                      {item.address_line_2}
                    </Text>
                  </Button>
                );
              })
            : null}
        </Block>

        <Block
          row
          style={{
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <Button
            color="transparent"
            onPress={() => {
              // navigation.goBack();
            }}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors.APP.primary,
              width: wp('10%'),
              height: wp('10%'),
              // borderRadius: wp('5%'),
              borderWidth: 0,
              padding: wp('2%'),
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
            }}>
            <Icon name="grid" color={Colors.APP.white} size={wp('4%')} />
          </Button>

          <Input
            placeholder="Search Shops, Categories..."
            left
            icon="search"
            family="feather"
            onChangeText={this.onChangeValue}
            value={searchQuery}
            iconSize={14}
            iconColor={Colors.APP.base}
            style={{
              width: wp('80%'),
              height: wp('10%'),
              borderTopLeftRadius: 0,
              borderBottomLeftRadius: 0,
              borderWidth: 0,
            }}
          />
          {/* <Button
            color={Colors.APP.base}
            onPress={() => {
              const filters = {
                search: searchQuery,
                isToday: isTodayOffer,
                category: choosenCategories[0] || activeCategory,
                location: activeLocation,
              };

              if (isNearMe) {
                this.props.navigation.navigate('NearMe', filters);
              } else {
                this.props.navigation.navigate('ListOffer', filters);
              }
            }}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContents: 'center',
              width: wp('10%'),
              height: wp('10%'),
              // borderRadius: wp('5%'),
              borderWidth: 0,
              padding: wp('2%'),

              borderTopRightRadius: 10,
              borderBottomRightRadius: 10,
              borderTopLeftRadius: 0,
              borderBottomLeftRadius: 0,
            }}>
            <Icon name="search" color={Colors.APP.white} size={wp('5%')} />
          </Button> */}
        </Block>

        <Block
          // row
          center
          style={{
            backgroundColor: 'white',
            position: 'absolute',
            top: 110,
            zIndex: 1000,
            width: wp('80%'),
            // marginTop: 10,
          }}>
          {categories.length
            ? categories.map((item, key) => {
                return this.renderItem(item);
              })
            : null}
        </Block>

        <Block
          row
          style={{
            // width: wp('80%'),
            marginVertical: hp('2%'),
            alignSelf: 'center',
            zIndex: 999,
            alignItems: 'center',
          }}>
          <Button
            size="small"
            color={Colors.APP.base}
            onPress={() => {
              const filters = {
                search: activeCategory ? null : searchQuery,
                category: activeCategory,
                location: locationQuery,
              };

              if (isNearMe) {
                this.props.navigation.navigate('NearMe', filters);
              } else {
                this.props.navigation.navigate('ListOffer', filters);
              }
            }}
            style={{
              alignSelf: 'center',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContents: 'center',
              borderRadius: wp('5%'),
              borderWidth: 0,
              // padding: wp('2%'),
              // marginVertical: hp('2%'),
            }}>
            <Icon name="search" color={Colors.APP.white} size={wp('5%')} />
          </Button>

          {searchQuery ||
          locationQuery ||
          categories.length > 0 ||
          locationSuggestion.length > 0 ? (
            <Button
              onPress={() => {
                this.setState({
                  choosenCategories: [],
                  isNearMe: false,
                  activeCategory: null,
                  activeLocation: null,
                  searchQuery: null,
                  locationQuery: null,
                  categories: [],
                  locationSuggestion: [],
                });
              }}
              color="white"
              size="small"
              style={{
                marginTop: hp('1%'),
                marginLeft: wp('1%'),
                width: wp('20%'),
                height: hp('2.5%'),
                borderWidth: 1,
                borderColor: Colors.APP.black,
                borderRadius: 30,
                elevation: 0,
                // marginLeft: 10,
                // position: 'absolute',
                // right: -100,
                // bottom: 10,
                // zIndex: 999,
              }}>
              <Text
                style={
                  {
                    // textDecorationLine: 'underline',
                  }
                }>
                Clear Search
              </Text>
            </Button>
          ) : (
            <Block
              style={{
                height: hp('3.5%'),
              }}
            />
          )}
        </Block>

        <Block
          style={{
            marginBottom: hp('2%'),
          }}
        />
      </Block>
    );
  }
}

export default Search;
