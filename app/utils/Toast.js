import React from 'react';
import {ToastAndroid} from 'react-native';

const Toast = ({visible, message}) => {
  if (visible) {
    ToastAndroid.showWithGravityAndOffset(message, ToastAndroid.LONG, ToastAndroid.TOP, 25, 50);
    return null;
  }
  return null;
};

export default Toast;
