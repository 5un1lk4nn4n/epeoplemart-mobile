import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';

export const StorageContext = React.createContext({
  name: '',
  avatar: '',
  mobile: '',
});

export class UserInfoProvider extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      token: '',
    };
    this.getUserInfo();
  }

  componentDidMount() {
    // this.getUserInfo();
  }

  // componentWillUnmount() {
  //   this.getUserInfo();
  // }

  getUserInfo = () => {
    this.getToken().then(value => {
      if (value) {
        this.setState({
          token: value,
        });
      }
    });
  };

  getToken = () => {
    const retrievedItem = AsyncStorage.getItem('@token');
    return retrievedItem;
  };

  getUsername = () => {
    const retrievedItem = AsyncStorage.getItem('@name');
    return retrievedItem;
  };

  getAvatar = () => {
    const avatar = AsyncStorage.getItem('@avatar');
    return avatar;
  };

  getMobile = () => {
    const mobile = AsyncStorage.getItem('@mobile');
    return mobile;
  };

  render() {
    const {children} = this.props;
    return (
      <StorageContext.Provider value={this.state}>
        {children}
      </StorageContext.Provider>
    );
  }
}

export const withUserInfoContext = ChildComponent => props => (
  <StorageContext.Consumer>
    {context => <ChildComponent {...props} global={context} />}
  </StorageContext.Consumer>
);
