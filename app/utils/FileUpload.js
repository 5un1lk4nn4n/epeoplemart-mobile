import React from 'react';
import {BreathingLoader} from 'react-native-indicator';
import {Block} from 'galio-framework';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Colors, Typography} from '../styles';
import AsyncStorage from '@react-native-community/async-storage';
import {MAIN_URL, URLS} from '../constants';

import RNFS from 'react-native-fs';
const {PROFILE_UPLOAD} = URLS;
const DIRECTORY = `${RNFS.ExternalStorageDirectoryPath}/Pictures`;

const FILE_TYPE = 'image/jpeg';

export class FileUpload extends React.PureComponent {
  constructor(props) {
    super(props);
    const {name, filepath} = this.props;
    (this.name = name), (this.path = filepath);
    (this.isUploading = false),
      (this.state = {
        uploadPercent: 0,
      });
    this.url = name === 'profile' ? `${MAIN_URL}${PROFILE_UPLOAD}` : null;
    this.getToken().then(token => {
      this.secret = token;
      this.uploadFile();
    });
  }

  componentDidMount() {}

  componentWillUnmount() {}

  uploadBegin = response => {
    var jobId = response.jobId;
    console.log('UPLOAD HAS BEGUN! JobId: ' + jobId);
  };

  getToken() {
    console.log('getting token');
    // Retrieves the user token from localStorage
    return AsyncStorage.getItem('@token');
  }
  uploadProgress = response => {
    var percentage = Math.floor(
      (response.totalBytesSent / response.totalBytesExpectedToSend) * 100,
    );
    this.setState({
      uploadPercent: percentage,
    });
  };

  uploadFile = () => {
    this.isUploading = true;

    RNFS.uploadFiles({
      toUrl: this.url,
      files: [
        {
          name: this.name,
          filepath: this.path,
          filetype: FILE_TYPE,
        },
      ],
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${this.secret}`,
      },

      begin: this.uploadBegin,
      progress: this.uploadProgress,
    })
      .promise.then(response => {
        if (response.statusCode == 200) {
          console.log('FILES UPLOADED!', response); // response.statusCode, response.headers, response.body
          this.deleteFile();
        } else {
          console.log('SERVER ERROR');
        }
      })
      .catch(err => {
        if (err.description === 'cancelled') {
          // cancelled by user
        }
        console.log(err);
      });
  };

  deleteFile = () => {
    // var path = RNFS.DocumentDirectoryPath + '/test.txt';

    RNFS.unlink(this.path)
      .then(() => {
        console.log('FILE DELETED');
      })
      // `unlink` will throw an error, if the item to unlink does not exist
      .catch(err => {
        console.log(err.message);
      });
  };

  render() {
    return (
      <Block flex={1} center middle>
        <BubblesLoader color={Colors.APP.base} size={wp('10%')} />
        <Text
          color={Colors.APP.base}
          style={{
            ...Typography.ff.bold,
          }}>{`Uploading ${this.state.uploadPercent}`}</Text>
      </Block>
    );
  }
}
