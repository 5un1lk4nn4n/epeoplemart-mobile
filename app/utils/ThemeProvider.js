import React from 'react';
import {GalioProvider} from 'galio-framework';

import {Colors} from '../styles';

const THEME = {
  SIZES: {BASE: 16},
  COLORS: {PRIMARY: Colors.APP.base, BASE: Colors.APP.base},
};

export default class ThemeProvider extends React.PureComponent {
  render() {
    const {children} = this.props;
    return <GalioProvider theme={THEME}>{children}</GalioProvider>;
  }
}
