import React, { Component } from 'react';
import { Image } from 'react-native';

const logo = require('../assets/images/logo-full.png');

export default class PerfectImage extends Component {
  source = logo;

  constructor() {
    super();
    this.state = {
      loaded: false,
      width: 0,
      height: 0,
    };
  }

  componentDidMount() {
    const { url, source } = this.props;
    if (url) {
      this.setRemoteSource(url);
      this.getRemoteImageSize(url);
    }
    if (source) {
      this.setLocalSource(source);
      this.getlocalImageSize(source);
    }
  }

  setLocalSource = source => {
    this.source = source;
  };

  setRemoteSource = source => {
    this.source = { uri: source };
  };

  getRemoteImageSize = url => {
    Image.getSize(url, (width, height) => {
      this.setImageSize(width, height);
    });
  };

  getlocalImageSize = source => {
    const { width, height } = Image.resolveAssetSource(source);
    this.setImageSize(width, height);
  };

  setImageSize = (width, height) => {
    this.setState({ width, height, loaded: true });
  };

  render() {
    const { loaded, width, height } = this.state;
    const { style } = this.props;
    return <Image source={this.source} style={{ width, height, ...style }} />;
  }
}
PerfectImage.defaultProps = {
  style: {},
};
