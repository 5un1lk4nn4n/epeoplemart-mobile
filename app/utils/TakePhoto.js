import React from 'react';
import {BreathingLoader} from 'react-native-indicator';
import {Block} from 'galio-framework';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Colors, Typography} from '../styles';
import AsyncStorage from '@react-native-community/async-storage';
import {MAIN_URL, URLS} from '../constants';
import ImagePicker from 'react-native-image-crop-picker';

import RNFS from 'react-native-fs';
const {PROFILE_UPLOAD} = URLS;
const DIRECTORY = `${RNFS.ExternalStorageDirectoryPath}/Pictures`;

const FILE_TYPE = 'image/jpeg';

export default class TakePhoto {
  constructor() {
    this.selectPhoto = this.selectPhoto.bind(this);
    this.deleteFile = this.deleteFile.bind(this);
  }

  deleteFile = path => {
    // var path = RNFS.DocumentDirectoryPath + '/test.txt';

    RNFS.unlink(this.path)
      .then(() => {
        console.log('FILE DELETED');
      })
      // `unlink` will throw an error, if the item to unlink does not exist
      .catch(err => {
        console.log(err.message);
      });
  };

  selectPhoto = type => {
    if (type === 'camera') {
      ImagePicker.openCamera({
        cropping: true,
        width: 500,
        height: 500,
        cropperCircleOverlay: true,
        compressImageMaxWidth: 640,
        compressImageMaxHeight: 480,
        freeStyleCropEnabled: true,
        includeBase64: true,
      })
        .then(image => {
          console.log(image);
          this.props.navigation.navigate('CheckIn', {
            selfie: image,
          });

          // this.storeUploadedData(item, image);
        })
        .catch(e => {
          console.log(e), this.setState({imageModalVisible: false});
        });

      console.log('camera');
    }
  };
}
