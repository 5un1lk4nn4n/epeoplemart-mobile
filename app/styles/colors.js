export const APP = {
  base: '#C9002B',
  primary: '#004B93',
  secondary: '#fff',
  light: '#edece6',
  white: '#fff',
  black: '#000',
  dark: '#797979',
  greyer: '#8c8c8c',
  grey: '#E8E8E8',
  delete: '#CD326B',
  plus: '#56AB54',
  minus: '#5456AB',
  highlight: '#e76e50',
  tertiary: '#007D8C',
};

export const bgPrimary = {
  backgroundColor: APP.primary,
};

export const bgDark = {
  backgroundColor: APP.dark,
};

export const bgSecondary = {
  backgroundColor: APP.secondary,
};

export const bgWhite = {
  backgroundColor: APP.white,
};

export const bgGrey = {
  backgroundColor: APP.grey,
};

export const bgGeyer = {
  backgroundColor: APP.greyer,
};

export const bgTranparent = {
  backgroundColor: 'transparent',
};
