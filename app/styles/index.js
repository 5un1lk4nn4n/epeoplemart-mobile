import * as Buttons from './buttons';
import * as Colors from './colors';
import * as Typography from './typography';
import * as Spacing from './spacing';

export {Typography, Colors, Buttons, Spacing};
