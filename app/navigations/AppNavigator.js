import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {View, Text} from 'react-native';
import Ripple from 'react-native-material-ripple';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/Feather';

import AuthLoadingScreen from '../screens/AuthLoadingScreen';
import Home from '../screens/Home';
import SignIn from '../screens/Login';
import Accounts from '../screens/Accounts';
import History from '../screens/History';
import QuickPay from '../screens/QuickPay';
import NearMe from '../screens/NearMe';
import Search from '../screens/Search';
import ListOffer from '../screens/ListOffers';
import SplashScreen from '../screens/SplashScreen';

import {Colors, Typography} from '../styles';
import ShopList from '../screens/ShopList';

const renderIcon = (routeName, focused) => {
  switch (routeName) {
    case 'Home':
    default:
      return (
        <Icon
          name="home"
          color={focused ? Colors.APP.base : '#666'}
          size={wp('4%')}
          style={{
            backgroundColor: focused ? 'rgba(201, 0, 43,0.2)' : '#fff',
            padding: wp('1%'),
            borderRadius: 2,
          }}
        />
      );
    case 'Activity':
      return (
        <Icon
          name="activity"
          size={wp('4%')}
          color={focused ? Colors.APP.base : '#666'}
          style={{
            backgroundColor: focused ? 'rgba(201, 0, 43,0.2)' : '#fff',
            padding: wp('1%'),
            borderRadius: 2,
          }}
        />
      );
    case 'Accounts':
      return (
        <Icon
          name="user"
          size={wp('4%')}
          color={focused ? Colors.APP.base : '#666'}
          style={{
            backgroundColor: focused ? 'rgba(201, 0, 43,0.2)' : '#fff',
            padding: wp('1%'),
            borderRadius: 2,
          }}
        />
      );
    case 'QuickPay':
      return (
        <Icon
          name="crosshair"
          size={wp('4%')}
          color={focused ? Colors.APP.base : '#666'}
          style={{
            backgroundColor: focused ? 'rgba(201, 0, 43,0.2)' : '#fff',
            padding: wp('1%'),
            borderRadius: 2,
          }}
        />
      );
    case 'NearMe':
      return (
        <Icon
          name="map-pin"
          size={wp('4%')}
          color={focused ? Colors.APP.base : '#666'}
          style={{
            backgroundColor: focused ? 'rgba(201, 0, 43,0.2)' : '#fff',
            padding: wp('1%'),
            borderRadius: 2,
          }}
        />
      );
    case 'Shops':
      return (
        <Icon
          name="database"
          size={wp('4%')}
          color={focused ? Colors.APP.base : '#666'}
          style={{
            backgroundColor: focused ? 'rgba(201, 0, 43,0.2)' : '#fff',
            padding: wp('1%'),
            borderRadius: 2,
          }}
        />
      );
  }
};

const renderNav = (routeName, name, tintColor, focused, navigation) => {
  return (
    <Ripple
      style={{
        flex: 1,
        width: wp('30%'),
      }}
      onPress={() => navigation.navigate(routeName)}>
      <View
        style={{
          flex: 1,
          width: wp('30%'),
          alignItems: 'center',
          paddingVertical: 6,
          // borderTopWidth: focused ? 2 : 2,
        }}>
        {renderIcon(routeName, focused, name)}
        <Text
          style={{
            paddingVertical: 5,
            color: focused ? tintColor : '#666666',
            ...Typography.ff.bold,
          }}>
          {name}
        </Text>
      </View>
    </Ripple>
  );
};

const customTabs = ({navigation}) => ({
  tabBarIcon: ({focused, horizontal, tintColor}) => {
    const {routeName} = navigation.state;
    if (routeName === 'Home') {
      return renderNav(routeName, 'Home', tintColor, focused, navigation);
    }
    if (routeName === 'Activity') {
      return renderNav(routeName, 'Purchases', tintColor, focused, navigation);
    }
    if (routeName === 'Accounts') {
      return renderNav(routeName, 'Accounts', tintColor, focused, navigation);
    }
    if (routeName === 'QuickPay') {
      return renderNav(routeName, 'Scan QR', tintColor, focused, navigation);
    }
    if (routeName === 'NearMe') {
      return renderNav(routeName, 'Near Me', tintColor, focused, navigation);
    }
    if (routeName === 'Shops') {
      return renderNav(routeName, 'Shops', tintColor, focused, navigation);
    }
  },
});

// Routes

const HomeStack = createStackNavigator({
  Dashboard: Home,
  Search: Search,
  ListOffer: ListOffer,
  History: History,
});

HomeStack.navigationOptions = ({navigation}) => {
  const currentRoute = navigation.state.routes[navigation.state.index];
  const {routeName} = currentRoute;

  let tabBarVisible = true;
  if (routeName !== 'Dashboard') {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

const AuthStack = createStackNavigator({
  SignIn,
  // AppIntro: AppIntroScreen,
});

const MainStack = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
    },
    NearMe: {
      screen: NearMe,
    },
    Shops: {
      screen: ShopList,
    },
    QuickPay: {
      screen: QuickPay,
    },
    Accounts: {
      screen: Accounts,
    },
  },
  {
    defaultNavigationOptions: customTabs,
    animationEnabled: true,
    swipeEnabled: true,
    tabBarPosition: 'bottom',
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: Colors.APP.base,
      inactiveTintColor: 'rgba(0,0,0,0.6)',
      showLabel: false,
      style: {
        // shadowColor: 'rgba(58,55,55,0.1)',
        // shadowOffset: {width: 0, height: 0},
        // shadowOpacity: 1,
        // shadowRadius: 15,
        // elevation: 3,
        borderTopColor: 'transparent',
        backgroundColor: '#fff',
        // borderTopLeftRadius: 20,
        // borderTopRightRadius: 20,
        height: 60,
      },
      activeTabStyle: {
        backgroundColor: 'white',
        borderBottomWidth: 4,
        borderColor: '#6C1D7C',
      },
    },
  },
);

export default createAppContainer(
  createSwitchNavigator(
    {
      Splash: SplashScreen,
      AuthLoading: AuthLoadingScreen,
      App: MainStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'Splash',
    },
  ),
);
