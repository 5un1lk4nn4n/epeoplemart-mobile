import React from 'react';
import {StyleSheet} from 'react-native';
import {Text, Block} from 'galio-framework';

import LottieView from 'lottie-react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Colors} from '../styles';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.APP.light,
  },
});

const offline = require('../assets/animations/offline.json');

export default class Offline extends React.Component {
  async componentDidMount() {
    this.animation.play();
  }

  render() {
    const {navigation} = this.props;
    return (
      <Block flex={1} style={styles.container}>
        <Block center middle flex={0.8}>
          <LottieView
            ref={animation => {
              this.animation = animation;
            }}
            style={{
              width: wp('20%'),
              height: wp('20%'),
            }}
            loop={false}
            source={offline}
          />
          <Text bold color="#999">
            No Internet Connection
          </Text>
        </Block>
      </Block>
    );
  }
}
