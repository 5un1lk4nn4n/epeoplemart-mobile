import React from 'react';
import {Image} from 'react-native';
import {Block, Text, NavBar, Button} from 'galio-framework';
import Icon from 'react-native-vector-icons/Feather';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Colors} from '../styles';
const logo = require('../assets/images/logo.png');
class TopNavBar extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
  }

  render() {
    const {navigation, token} = this.props;
    return (
      <NavBar
        transparent
        title=""
        left={
          <Block row>
            <Image
              resizeMode="contain"
              source={logo}
              style={{
                width: wp('40%'),
                height: wp('8%'),
              }}
            />
            {/* <Button
              onPress={() => {
                alert('hi');
              }}
              // color="#c9002b1f"
              round
              shadowless
              style={{
                marginLeft: wp('2%'),
                border: 0,
                width: wp('65%'),
                flexDirection: 'row',
                justifyContent: 'flex-start',
                paddingLeft: wp('2%'),
              }}>
              <Icon name="search" color={Colors.APP.white} size={wp('4%')} />
              <Text
                color={Colors.APP.white}
                style={{
                  paddingLeft: wp('2%'),
                }}>
                Search here
              </Text>
            </Button> */}
          </Block>
        }
        right={
          <Button
            color="transparent"
            onPress={() => {
              if (token) {
                navigation.navigate('Accounts');
              } else {
                navigation.navigate('SignIn');
              }
            }}
            style={{
              // backgroundColor: Colors.APP.base,
              width: wp('10%'),
              borderRadius: wp('5%'),
              borderWidth: 0,
            }}>
            <Icon name="user" color={Colors.APP.primary} size={wp('5%')} />
          </Button>
        }
      />
    );
  }
}

export default TopNavBar;
