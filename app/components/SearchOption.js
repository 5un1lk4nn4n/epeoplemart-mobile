import React from 'react';
import {Image} from 'react-native';
import {Block, Text, NavBar, Button, Input} from 'galio-framework';
import Icon from 'react-native-vector-icons/Feather';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Colors} from '../styles';
const logo = require('../assets/images/logo.png');
class SearchOption extends React.Component {
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
  }

  render() {
    return (
      <Block flex={1} center>
        <Input
          placeholder="Input with Icon on right"
          right
          icon="search"
          family="feather"
          iconSize={14}
          iconColor={Colors.APP.base}
          style={{
            width: wp('90%'),
            borderWidth: 0,
          }}
        />
      </Block>
    );
  }
}

export default SearchOption;
