import React from 'react';
import {StyleSheet} from 'react-native';
import {Text, Block} from 'galio-framework';

import LottieView from 'lottie-react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Colors} from '../styles';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.APP.light,
  },
});

const defaultLoader = require('../assets/animations/loader.json');
const mapLoader = require('../assets/animations/map.json');

export default class Offline extends React.Component {
  async componentDidMount() {
    this.animation.play();
  }

  render() {
    const {map} = this.props;
    return (
      <Block flex={1} style={styles.container}>
        <Block center middle flex={1}>
          <LottieView
            ref={animation => {
              this.animation = animation;
            }}
            style={{
              width: wp('50%'),
              height: wp('50%'),
            }}
            // loop={false}
            source={map ? mapLoader : defaultLoader}
          />
        </Block>
      </Block>
    );
  }
}
