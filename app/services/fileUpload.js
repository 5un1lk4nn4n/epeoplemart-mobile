/* eslint-disable no-new */
/* eslint-disable class-methods-use-this */
import AsyncStorage from '@react-native-community/async-storage';
import {MAIN_URL, URLS} from '../constants';

import RNFS from 'react-native-fs';
import {file} from '@babel/types';
const {UPLOAD} = URLS;
const DIRECTORY = `${RNFS.ExternalStorageDirectoryPath}/Pictures`;

const FILE_TYPE = 'image/jpeg';
export default class FileUpload {
  // Initializing important variables
  secret = null;

  constructor() {
    this.avatar = this.avatar.bind(this);
  }

  avatar = (name, filepath, mime, callback) => {
    const filename = filepath.split('Pictures/');
    this.name = name;
    this.filename = filename[1];
    this.path = RNFS.PicturesDirectoryPath + '/' + this.filename;
    // this.path = filepath;
    this.mime = mime;
    this.callback = callback;
    this.url = `${MAIN_URL}${UPLOAD}`;
    this.getToken().then(token => {
      this.secret = token;
      this.uploadFile();
    });
  };
  uploadBegin = response => {
    var jobId = response.jobId;
    console.log('UPLOAD HAS BEGUN! JobId: ' + jobId);
  };

  getToken() {
    console.log('getting token');
    // Retrieves the user token from localStorage
    return AsyncStorage.getItem('@token');
  }
  uploadProgress = response => {
    var percentage = Math.floor(
      (response.totalBytesSent / response.totalBytesExpectedToSend) * 100,
    );
    console.log(percentage, '%');
  };

  uploadFile = () => {
    this.isUploading = true;

    RNFS.uploadFiles({
      toUrl: this.url,
      files: [
        {
          name: this.name,
          filename: this.name,
          filepath: this.path,
          filetype: FILE_TYPE,
        },
      ],
      fields: {
        imageSize: '100',
      },
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${this.secret}`,
      },

      begin: this.uploadBegin,
      progress: this.uploadProgress,
    })
      .promise.then(response => {
        if (response.statusCode == 200) {
          console.log('FILES UPLOADED!', response); // response.statusCode, response.headers, response.body
          this.deleteFile();
        } else {
          console.log('SERVER ERROR');
        }
      })
      .catch(err => {
        if (err.description === 'cancelled') {
          // cancelled by user
        }
        console.log(err);
      });
  };

  deleteFile = () => {
    // var path = RNFS.DocumentDirectoryPath + '/test.txt';

    RNFS.unlink(this.path)
      .then(() => {
        console.log('FILE DELETED');
      })
      // `unlink` will throw an error, if the item to unlink does not exist
      .catch(err => {
        console.log(err.message);
      });
  };
}
