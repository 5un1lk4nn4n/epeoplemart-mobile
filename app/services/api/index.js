/* eslint-disable no-new */
/* eslint-disable class-methods-use-this */
import decode from 'jwt-decode';
import AsyncStorage from '@react-native-community/async-storage';
import {MAIN_URL, ACCOUNTS_URL, URLS} from '../../constants';

const {
  HOME,
  QUICK_PAY,
  CATEGORIES,
  SUGGESTION,
  SEARCH,
  NEARME,
  LOGIN,
  VENDOR_VIEWS,
  VENDOR,
} = URLS;
export default class ApiService {
  // Initializing important variables

  isTokenExists = true;
  constructor() {
    this.secret = null;
    this.domain = MAIN_URL;
    this.fetch = this.fetch.bind(this);

    this.tokenVerify = this.tokenVerify.bind(this);

    this.getToken().then(token => {
      if (!token) {
        isTokenExists = false;
      }
      this.secret = token;
    });
  }

  getHome = () => {
    return this.fetch(`${this.domain}${HOME}`).then(res => {
      return Promise.resolve(res);
    });
  };

  getCategories = filter => {
    return this.fetch(`${this.domain}${CATEGORIES}?${filter}`).then(res => {
      return Promise.resolve(res);
    });
  };

  getVendors = filter => {
    return this.fetch(`${this.domain}${VENDOR}?${filter}`).then(res => {
      return Promise.resolve(res);
    });
  };

  autoSuggestion = search => {
    // Get a token from api server using the fetch api
    return this.fetch(`${this.domain}${SUGGESTION}`, {
      method: 'POST',
      body: JSON.stringify(search),
    }).then(res => {
      return Promise.resolve(res);
    });
  };
  nearMe = location => {
    // Get a token from api server using the fetch api
    return this.fetch(`${this.domain}${NEARME}`, {
      method: 'POST',
      body: JSON.stringify(location),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  searchOffers = search => {
    // Get a token from api server using the fetch api
    return this.fetch(`${this.domain}${SEARCH}`, {
      method: 'POST',
      body: JSON.stringify(search),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  vendorViewCount = vendor_id => {
    return this.fetch(`${this.domain}${VENDOR_VIEWS}`, {
      method: 'POST',
      body: JSON.stringify({
        vendor_id,
      }),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  login = (password, mobile) => {
    // Get a token from api server using the fetch api
    return this.fetch(`${ACCOUNTS_URL}${LOGIN}`, {
      method: 'POST',
      body: JSON.stringify({
        mobile,
        password,
      }),
    }).then(res => {
      return Promise.resolve(res);
    });
  };

  async quickPay(code) {
    // Get a token from api server using the fetch api
    const userToken = await AsyncStorage.getItem('@token');
    if (!userToken) {
      return Promise.resolve({
        is_authenticated: false,
      });
    }
    return this.fetch(`${this.domain}${QUICK_PAY}`, {
      method: 'POST',
      body: JSON.stringify({
        code,
      }),
    }).then(res => {
      return Promise.resolve(res);
    });
  }

  tokenVerify(token) {
    // Get a token from api server using the fetch api
    return this.fetch(`${this.domain}${Urls.URLS.tokenVerify}`, {
      method: 'POST',
      body: JSON.stringify({
        token,
      }),
    }).then(res => {
      return Promise.resolve(res);
    });
  }

  loggedIn() {
    // alert(this.secret);
    // alert(!!this.secret);
    // Checks if there is a saved token and it's still valid
    // const token = this.getToken(); // GEtting token from localstorage
    // return !!this.secret && !this.isTokenExpired(this.secret); // handwaiving here
    return !!this.secret; // handwaiving here
  }

  isTokenExpired(token) {
    try {
      this.decoded = decode(token);
      if (this.decoded.exp < Date.now() / 1000) {
        // Checking if token is expired. N
        return true;
      }
      return false;
    } catch (err) {
      return false;
    }
  }

  setToken(data) {
    // Saves user token to localStorage
    AsyncStorage.setItem('@token', data);
  }

  getToken() {
    // Retrieves the user token from localStorage
    return AsyncStorage.getItem('@token');
  }

  logout() {
    // Clear user token and profile data from localStorage
    localStorage.removeItem('ut');
  }

  getProfile() {
    // Using jwt-decode npm package to decode the token
    return decode(this.getToken());
  }

  bootstrapAsync = async () => {
    return await AsyncStorage.getItem('@token');
  };

  async fetch(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    const userToken = await AsyncStorage.getItem('@token');
    console.log(userToken);
    this.secret = userToken;
    // this.secret = Task.Run(this.bootstrapAsync()).Result;

    // Setting Authorization header
    // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx

    if (this.loggedIn()) {
      headers.Authorization = `Bearer ${this.secret}`;
    } else {
    }

    return (
      fetch(url, {
        headers,
        ...options,
      })
        // .then(this.checkStatus)
        .then(response => response.json())
        .catch(error => error)
    );
    // .catch(error.)
  }

  // eslint-disable-next-line consistent-return
  checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      // Success status lies between 200 to 300
      return response;
    }
    // eslint-disable-next-line prefer-const
    let error = new Error(response.statusText);
    // return response;
    return Promise.reject(error);
    // error.response = response;
    // throw error;
  }
}
